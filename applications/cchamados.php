<?php

class cchamados extends app {

    private $prefix = false;

    private function get($name) {
        if ($this->prefix) {
            return $this->{"{$this->prefix}_{$name}"};
        }
        return $this->$name;
    }

    private function set($name, $value) {
        if ($this->prefix) {
            return $this->{"{$this->prefix}_{$name}"} = $value;
        }
        return $this->$name = $value;
    }

    public function __construct($id = false, $prefix = false) {
        $this->prefix = $prefix;
        if ($id) {
            $db = new mysqlsearch();
            $db->table("chamados_view");
            $db->column("*");
            $db->match("id", $id);
            $db = $db->go();
            if (isset($db[0])) {
                $this->extract($db[0], $this->prefix);
                $this->set("servicos", self::lista_servicos($id));
                $this->set("produtos", self::lista_produtos($id));
            }
        }
    }

    public function salvar($id = false) {
        if (isset($_POST["servicos"])) {
            if (is_array($_POST["servicos"])) {
                if (isset($_POST["produtos"])) {
                    if (is_array($_POST["produtos"])) {
                        $this->extract($_POST, $this->prefix);
                        $db = new mysqlsave();
                        $db->table("chamados");
                        $db->column("empresa", $this->get("empresa"));
                        $db->column("cnpj", $this->get("cnpj"));
                        $db->column("telefone", $this->get("telefone"));
                        $db->column("endereco_logradouro", $this->get("endereco_logradouro"));
                        $db->column("endereco_complemento", $this->get("endereco_complemento"));
                        $db->column("endereco_bairro", $this->get("endereco_bairro"));
                        $db->column("endereco_cidade", $this->get("endereco_cidade"));
                        $db->column("endereco_estado", $this->get("endereco_estado"));
                        $db->column("endereco_cep", $this->get("endereco_cep"));
                        $db->column("agenda_data", knife::date_converter($this->get("agenda_data"), "/", "-"));
                        $db->column("agenda_hora_ini", $this->get("agenda_hora_ini"));
                        $db->column("agenda_hora_fim", $this->get("agenda_hora_fim"));
                        $db->column("bancos_id", $this->get("bancos_id"));
                        $db->column("usuarios_id", $_SESSION["usuario"]["id"]);
                        $db->column("anotacoes", $this->get("anotacoes"));
                        $db->column("atendimento_data", knife::date_converter($this->get("atendimento_data"), "/", "-"));
                        $db->column("atendimento_hora_ini", $this->get("atendimento_hora_ini"));
                        $db->column("atendimento_hora_fim", $this->get("atendimento_hora_fim"));
                        $db->column("visita_status", $this->get("visita_status"));
                        $db->column("tecnico", $this->get("tecnico"));
                        $db->column("observacao", $this->get("observacao"));
                        if ($id) {
                            $db->match("id", $id);
                        } else {
                            $db->column("datacadastro", date("Y-m-d H:i:s"));
                        }
                        if ($db->go()) {
                            if (!$id) {
                                $id = $db->id();
                            } else {
                                $db = new mysqldelete();
                                $db->table("servicos_has_chamados");
                                $db->match("chamados_id", $id);
                                $db->go();
                                $db = new mysqldelete();
                                $db->table("produtos_has_chamados");
                                $db->match("chamados_id", $id);
                                $db->go();
                            }
                            $db = new mysqlsave();
                            $db->table("chamados");
                            $db->column("numero", "S" . str_pad($id, 7, '0', STR_PAD_LEFT));
                            $db->match("id", $id);
                            $db->go();
                            foreach ($_POST["servicos"] as $value) {
                                if (strlen(trim($value)) > 0) {
                                    $db = new mysqlsave();
                                    $db->table("servicos_has_chamados");
                                    $db->column("servicos_id", $value);
                                    $db->column("chamados_id", $id);
                                    $db->go();
                                }
                            }
                            foreach ($_POST["produtos"] as $value) {
                                if (strlen(trim($value)) > 0) {
                                    $db = new mysqlsave();
                                    $db->table("produtos_has_chamados");
                                    $db->column("produtos_id", $value);
                                    $db->column("chamados_id", $id);
                                    $db->go();
                                }
                            }
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public function abrir() {
        //dump($_POST);
        //return false;
        if (isset($_POST["servicos"])) {
            if (is_array($_POST["servicos"])) {
                if (isset($_POST["produtos"])) {
                    if (is_array($_POST["produtos"])) {
                        $this->extract($_POST, $this->prefix);
                        $db = new mysqlsave();
                        $db->table("chamados");
                        $db->column("empresa", $this->get("empresa"));
                        $db->column("cnpj", $this->get("cnpj"));
                        $db->column("telefone", $this->get("telefone"));
                        $db->column("endereco_logradouro", $this->get("endereco_logradouro"));
                        $db->column("endereco_complemento", $this->get("endereco_complemento"));
                        $db->column("endereco_bairro", $this->get("endereco_bairro"));
                        $db->column("endereco_cidade", $this->get("endereco_cidade"));
                        $db->column("endereco_estado", $this->get("endereco_estado"));
                        $db->column("endereco_cep", $this->get("endereco_cep"));
                        $db->column("agenda_data", knife::date_converter($this->get("agenda_data"), "/", "-"));
                        $db->column("agenda_hora_ini", $this->get("agenda_hora_ini"));
                        $db->column("agenda_hora_fim", $this->get("agenda_hora_fim"));
                        $db->column("bancos_id", $this->get("bancos_id"));
                        $db->column("usuarios_id", $_SESSION["usuario"]["id"]);
                        $db->column("anotacoes", $this->get("anotacoes"));
                        if ($db->go()) {
                            $id = $db->id();
                            $db = new mysqlsave();
                            $db->table("chamados");
                            $db->column("numero", "S" . str_pad($id, 7, '0', STR_PAD_LEFT));
                            $db->match("id", $id);
                            $db->go();
                            foreach ($_POST["servicos"] as $value) {
                                if (strlen(trim($value)) > 0) {
                                    $db = new mysqlsave();
                                    $db->table("servicos_has_chamados");
                                    $db->column("servicos_id", $value);
                                    $db->column("chamados_id", $id);
                                    $db->go();
                                }
                            }
                            foreach ($_POST["produtos"] as $value) {
                                if (strlen(trim($value)) > 0) {
                                    $db = new mysqlsave();
                                    $db->table("produtos_has_chamados");
                                    $db->column("produtos_id", $value);
                                    $db->column("chamados_id", $id);
                                    $db->go();
                                }
                            }
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public function fechar($id = false) {
        if ($id) {
            $this->extract($_POST, $this->prefix);
            if ($this->get("atendimento_data") and $this->get("atendimento_hora_ini") and $this->get("atendimento_hora_fim") and $this->get("tecnico")) {
                $db = new mysqlsave();
                $db->table("chamados");
                $db->column("atendimento_data", knife::date_converter($this->get("atendimento_data"), "/", "-"));
                $db->column("atendimento_hora_ini", $this->get("atendimento_hora_ini"));
                $db->column("atendimento_hora_fim", $this->get("atendimento_hora_fim"));
                $db->column("visita_status", $this->get("visita_status"));
                $db->column("tecnico", $this->get("tecnico"));
                $db->column("observacao", $this->get("observacao"));
                $db->column("status", '2');
                $db->match("id", $id);
                return $db->go();
            }
        }
        return false;
    }

    public function apagar($id = false) {
        if ($id) {
            $db = new mysqlsave();
            $db->table("chamados");
            $db->column("status", '0');
            $db->match("id", $id);
            return $db->go();
        }
        return false;
    }

    static function lista($busca = false, $ordenacao = 2, $ordem = 'ASC') {
        $db = new mysqlsearch();
        $db->table("chamados_view");
        $db->column("id");
        $db->column("numero");
        $db->column("empresa");
        $db->column("cnpj");
        $db->column("telefone");
        $db->column("endereco_logradouro");
        $db->column("endereco_complemento");
        $db->column("endereco_bairro");
        $db->column("endereco_cidade");
        $db->column("endereco_estado");
        $db->column("endereco_cep");
        $db->column("anotacoes");
        $db->column("agenda_data");
        $db->column("agenda_hora_ini");
        $db->column("agenda_hora_fim");
        $db->column("atendimento_data");
        $db->column("atendimento_hora_ini");
        $db->column("atendimento_hora_fim");
        $db->column("visita_status");
        $db->column("tecnico");
        $db->column("observacao");
        $db->column("datacadastro", 1, "datacadastro_db");
        $db->column("datamodificado", 1, "datamodificado_db");
        $db->column("datacadastro_br", 1, "datacadastro");
        $db->column("datamodificado_br", 1, "datamodificado");
        $db->column("bancos_id");
        $db->column("usuarios_id");
        $db->column("agenda");
        $db->column("atendimento");
        $db->column("agenda_br");
        $db->column("atendimento_br");
        $db->column("banco");
        $db->column("usuarios_nome");
        $db->column("status_chamado");
        if ($busca) {
            $db->like("numero", $busca);
            $db->like("empresa", $busca, "OR");
            $db->like("cnpj", $busca, "OR");
            $db->like("telefone", $busca, "OR");
            $db->like("status_chamado", $busca, "OR");
        }
        $db->order($ordenacao, $ordem);
        return $db->go();
    }

    static function lista_produtos($chamados_id, $ordenacao = 1, $ordem = 'DESC') {
        $db = new mysqlsearch();
        $db->table("chamados_produtos_view");
        $db->column("produto");
        $db->column("produtos_id", 1, "id");
        $db->match("chamados_id", $chamados_id);
        $db->order($ordenacao, $ordem);
        return $db->go();
    }

    static function lista_servicos($chamados_id, $ordenacao = 1, $ordem = 'DESC') {
        $db = new mysqlsearch();
        $db->table("chamados_servicos_view");
        $db->column("servico");
        $db->column("servicos_id", 1, "id");
        $db->match("chamados_id", $chamados_id);
        $db->order($ordenacao, $ordem);
        return $db->go();
    }

}

?>
