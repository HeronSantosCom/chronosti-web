<?php

class cprodutos extends app {
    
    private $prefix = false;
    
    private function get($name) {
        if ($this->prefix) {
            return $this->{"{$this->prefix}_{$name}"};
        }
        return $this->$name;
    }

    public function __construct($id = false, $prefix = false) {
        $this->prefix = $prefix;
        if ($id) {
            $db = new mysqlsearch();
            $db->table("produtos");
            $db->column("*");
            $db->match("id", $id);
            $db = $db->go();
            if (isset($db[0])) {
                $this->extract($db[0], $this->prefix);
            }
        }
    }
    
    public function salvar($id = false) {
        $this->extract($_POST, $this->prefix);
        $db = new mysqlsave();
        $db->table("produtos");
        $db->column("produto", $this->get("produto"));
        if ($id) {
            $db->match("id", $id);
        }
        return $db->go();
    }

    public function apagar($id = false) {
        if ($id) {
            $db = new mysqldelete();
            $db->table("produtos");
            $db->match("id", $id);
            return $db->go();
        }
        return false;
    }

    static function lista($busca = false, $ordenacao = 2, $ordem = 'ASC') {
        $db = new mysqlsearch();
        $db->table("produtos");
        $db->column("id");
        $db->column("produto");
        if ($busca) {
            $db->like("produto", $busca);
        }
        $db->order($ordenacao, $ordem);
        return $db->go();
    }


}

?>
