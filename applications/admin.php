<?php

class admin extends app {

    public function __construct() {
        $this->extract($_GET);
        if (isset($_GET["reset"])) {
            $this->reset();
        }
    }

    public function login() {
        if (!isset($_SESSION["usuario"])) {
            $this->extract($_POST);
            if (isset($_COOKIE["username"])) {
                if (strlen($_COOKIE["username"]) > 0) {
                    $this->extract($_COOKIE);
                }
            }
            if ($this->username) {
                $usuarios = new mysqlsearch();
                $usuarios->table("usuarios_view");
                $usuarios->column("*");
                $usuarios->match("usuario", $this->username);
                $usuarios->match("senha", md5($this->password), "AND");
                $usuarios = $usuarios->go();
                if (isset($usuarios[0])) {
                    if ($this->remember) {
                        setcookie("username", $this->username, time() + 60 * 60 * 24 * 100, "/");
                        setcookie("password", $this->password, time() + 60 * 60 * 24 * 100, "/");
                    }
                    $usuarios[0]["avatar"] = knife::gravatar($usuarios[0]["email"], 60);
                    $_SESSION["usuario"] = $usuarios[0];
                    $this->historico("Login efetuado com sucesso.");
                    knife::redirect("painel.html");
                }
                $this->historico("Problemas ao efetuar login.");
                $this->mensagem = knife::html("includes/index/erro.html");
            }
            return false;
        }
        knife::redirect("painel.html");
    }

    public function senha() {
        if (!isset($_SESSION["usuario"])) {
            $this->extract($_POST);
            if ($this->email) {
                $usuarios = new mysqlsearch();
                $usuarios->table("usuarios_view");
                $usuarios->column("*");
                $usuarios->match("email", $this->email);
                $usuarios = $usuarios->go();
                if (isset($usuarios[0])) {
                    $senha = substr(md5(date("r")), -6);
                    $email[] = "Olá {$usuarios[0]["nome"]},";
                    $email[] = "";
                    $email[] = "Como solicitado, estamos enviando sua nova senha de acesso!";
                    $email[] = "Por segurança, anote em um local seguro.";
                    $email[] = "";
                    $email[] = "\tSeu usuário: {$usuarios[0]["usuario"]}";
                    $email[] = "\tSua nova senha é: {$senha}";
                    $email[] = "\tEndereço de acesso: http://www.chronos-ti.com.br/admin/";
                    $email[] = "";
                    $email[] = "Lembre-se, esta é uma senha temporária, altere se assim desejar!";
                    $email[] = "";
                    $email[] = name;
                    $email[] = "http://www.chronos-ti.com.br/";
                    $email[] = "";
                    $email[] = "";
                    $email[] = "";
                    $email[] = "";
                    $email[] = "";
                    $email[] = "=============================";
                    $email[] = "Esta é uma mensagem automática pelo S.I.G.E., por favor não responder!";
                    $email[] = "IP registrado: {$_SERVER["REMOTE_ADDR"]}";
                    $email[] = "Data de emissão: " . date("r");
                    $email[] = "";
                    $headers = "From: S.I.G.E. - " . strtoupper(name) . " <suporte@chronos-ti.com.br>\n";
                    $headers .= "Reply-To: S.I.G.E. - " . strtoupper(name) . " <suporte@chronos-ti.com.br>\n";
                    if (knife::mail_utf8($usuarios[0]["email"], '[S.I.G.E. - ' . strtoupper(name) . '] Sua nova senha!', htmlspecialchars(join("\n", $email)), $headers)) {
                        $save = new mysqlsave();
                        $save->table("usuarios");
                        $save->column("senha", md5($senha));
                        $save->match("id", $usuarios[0]["id"]);
                        $save = $save->go();
                        $this->mensagem = knife::html("includes/senha/sucesso.html");
                        $this->historico("Senha redefinida com sucesso.");
                        return true;
                    }
                }
                $this->mensagem = knife::html("includes/senha/erro.html");
                $this->historico("Problemas ao redefinir senha.");
            }
            return false;
        }
        knife::redirect("painel.html");
    }

    public function logoff() {
        if ($this->logged()) {
            $this->historico("Logoff efetuado com sucesso.");
            $this->reset();
        }
    }

    public function reset() {
        unset($_SESSION["usuario"]);
        unset($_SESSION["adm_content"]);
        if (isset($_COOKIE["username"])) {
            if (strlen($_COOKIE["username"]) > 0) {
                setcookie("username", false, time() + 60 * 60 * 24 * 100, "/");
                setcookie("password", false, time() + 60 * 60 * 24 * 100, "/");
            }
        }
        knife::redirect("index.html");
    }

    public function content($content = "dashboard") {
        if ($this->logged()) {
            if ($this->content) {
                $content = $this->content;
                $adm_content = new mysqlsearch();
                $adm_content->table("adm_content_view");
                $adm_content->column("*");
                $adm_content->match("content", $content);
                $adm_content->morethan("adm_content_nivel_id", $_SESSION["usuario"]["adm_content_nivel_id"], "AND");
                $adm_content = $adm_content->go();
                if (!isset($adm_content[0])) {
                    $content = "bloqueado.html";
                }
                $_SESSION["adm_content"] = $adm_content[0];
            }
            $this->loaded = $content;
            $this->menu = knife::html("includes/painel/menu.html");
            $this->content = knife::html("includes/painel/content/{$content}.html");
            $this->historico("Carregou página.");
            return true;
        }
    }

    public function logged() {
        if (isset($_SESSION["usuario"])) {
            return true;
        }
        knife::redirect("index.html");
    }

    //
    // Dashboard 
    //
    public function dashboard() {
        if ($this->logged()) {
            
        }
    }

    //
    // Menu 
    //
    public function menu() {
        if ($this->logged()) {
            $this->menu = cadm_content::lista($_SESSION["usuario"]["adm_content_nivel_id"], false, true);
        }
    }

    //
    // Listagem na Grid 
    //
    public function lista_usuarios_grid() {
        if ($this->logged()) {
            return cusuarios::lista($_SESSION["usuario"]["id"], $_SESSION["usuario"]["adm_content_nivel_id"]);
        }
    }

    public function lista_chamados_grid() {
        if ($this->logged()) {
            return cchamados::lista();
        }
    }

    //
    // Carrega informações para edição
    //
    public function edita_usuario() {
        if ($this->logged()) {
            $usuarios = new cusuarios($this->id);
            if (isset($_POST["submit"])) {
                if ($_POST["senha"] == $_POST["confirma_senha"]) {
                    if ($usuarios->salvar($this->id)) {
                        $this->historico("Usuário salvo com sucesso.");
                        knife::redirect("painel.html?content=usuarios");
                    } else {
                        $this->mensagem = "Ocorreu um erro ao salvar o usuário.<br />É possível que campos obrigatórios não foram preenchidos ou estão no formato inválido!";
                    }
                } else {
                    $this->mensagem = "As senhas não conferem!";
                }
                $this->historico("Erro ao salvar usuário.");
            }
            $this->adm_content_nivel = cadm_content_nivel::lista($_SESSION["usuario"]["adm_content_nivel_id"]);
        }
    }

    public function edita_conta() {
        if ($this->logged()) {
            $usuarios = new cusuarios($_SESSION["usuario"]["id"]);
            if (isset($_POST["submit"])) {
                if ($_POST["senha"] == $_POST["confirma_senha"]) {
                    $_POST["usuario"] = false;
                    $_POST["adm_content_nivel_id"] = false;
                    if ($usuarios->salvar($_SESSION["usuario"]["id"])) {
                        $this->historico("Usuário salvo com sucesso.");
                        $this->reset();
                    } else {
                        $this->mensagem = "Ocorreu um erro ao salvar suas informações.<br />É possível que campos obrigatórios não foram preenchidos ou estão no formato inválido!";
                    }
                } else {
                    $this->mensagem = "As senhas não conferem!";
                }
                $this->historico("Erro ao salvar usuário.");
            }
            $this->adm_content_nivel = cadm_content_nivel::lista($_SESSION["usuario"]["adm_content_nivel_id"]);
        }
    }

    public function edita_chamado() {
        if ($this->logged()) {
            $chamados = new cchamados($this->id);
            if (isset($_POST["submit"])) {
                if ($chamados->salvar($this->id)) {
                    $this->historico("Chamado salvo com sucesso.");
                    knife::redirect("painel.html?content=chamados");
                } else {
                    $this->mensagem = "Ocorreu um erro ao salvar o chamado.<br />É possível que campos obrigatórios não foram preenchidos ou estão no formato inválido!";
                }
                $this->historico("Erro ao salvar chamado.");
            }
            $this->bancos_lista = cbancos::lista();
            $this->servicos_lista = cservicos::lista();
            $this->produtos_lista = cprodutos::lista();
            $this->visita_status_lista = self::visita_status_lista();
        }
    }

    //
    // Carrega informações para abrir o chamado
    //

    public function abrir_chamado() {
        if ($this->logged()) {
            $chamados = new cchamados();
            if (isset($_POST["submit"])) {
                if ($chamados->abrir()) {
                    $this->historico("Chamado criado com sucesso.");
                    knife::redirect("painel.html?content=chamados");
                } else {
                    $this->mensagem = "Ocorreu um erro ao criar chamado.<br />É possível que campos obrigatórios não foram preenchidos ou estão no formato inválido!";
                }
                $this->historico("Erro ao criar chamado.");
            }
            $this->bancos_lista = cbancos::lista();
            $this->servicos_lista = cservicos::lista();
            $this->produtos_lista = cprodutos::lista();
            $this->visita_status_lista = self::visita_status_lista();
        }
    }

    //
    // Carrega informações para fechar o chamado
    //
    
    public function fecha_chamado() {
        if ($this->logged()) {
            $chamados = new cchamados($this->id);
            if (isset($_POST["submit"])) {
                if ($chamados->fechar($this->id)) {
                    $this->historico("Chamado fechado com sucesso.");
                    knife::redirect("painel.html?content=chamados");
                } else {
                    $this->mensagem = "Ocorreu um erro ao fechar a chamado.<br />Tente novamente mais tarde!";
                }
                $this->historico("Erro ao fechar chamado.");
            }
            $this->visita_status_lista = self::visita_status_lista();
        }
    }

    //
    // Carrega informações para remoção
    //
    public function remove_usuario() {
        if ($this->logged()) {
            $usuarios = new cusuarios($this->id);
            if (isset($_POST["submit"])) {
                if ($_SESSION["usuario"]["id"] != $this->id) {
                    if ($usuarios->apagar($this->id)) {
                        $this->historico("Usuário removido com sucesso.");
                        knife::redirect("painel.html?content=usuarios");
                    } else {
                        $this->mensagem = "Ocorreu um erro ao remover o usuário.<br />Tente novamente mais tarde!";
                    }
                } else {
                    $this->mensagem = "Você não tem permissão para excluir este usuário!";
                }
                $this->historico("Erro ao remover usuário.");
            }
        }
    }

    public function remove_chamado() {
        if ($this->logged()) {
            $chamados = new cchamados($this->id);
            if (isset($_POST["submit"])) {
                if ($chamados->apagar($this->id)) {
                    $this->historico("Chamado removido com sucesso.");
                    knife::redirect("painel.html?content=chamados");
                } else {
                    $this->mensagem = "Ocorreu um erro ao remover a chamado.<br />Tente novamente mais tarde!";
                }
                $this->historico("Erro ao remover chamado.");
            }
        }
    }

    //
    // Outros
    //

    static function visita_status_lista() {
        $visita_status[] = array("id" => '1', "status" => "Sim");
        $visita_status[] = array("id" => '2', "status" => "Não");
        $visita_status[] = array("id" => '3', "status" => "Cancelada");
        return $visita_status;
    }

    public function historico($acao) {
        $db = new mysqlsave();
        $db->table("historico");
        $db->column("acao", $acao);
        $db->column("server_variables", serialize($_SERVER));
        $db->column("request_variables", (isset($_REQUEST) ? serialize($_REQUEST) : null));
        $db->column("session_variables", (isset($_SESSION) ? serialize($_SESSION) : null));
        $db->column("cookie_variables", (isset($_COOKIE) ? serialize($_COOKIE) : null));
        $db->column("usuarios_id", (isset($_SESSION["usuario"]["id"]) ? $_SESSION["usuario"]["id"] : null));
        $db->column("adm_content_id", (isset($_SESSION["adm_content"]["id"]) ? $_SESSION["adm_content"]["id"] : null));
        return $db->go();
    }

}

?>