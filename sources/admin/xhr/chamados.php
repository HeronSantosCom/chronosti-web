<?php

//if (!class_exists("fastjson", false)) {
//    include path::common("fastjson/fastjson.php");
//}

$admin = new admin();
$listagem = $admin->lista_chamados_grid();

$array["aaData"] = false;
if ($listagem) {
    //$array["sEcho"] = intval($_GET['sEcho']);
    //$array["iTotalRecords"] = count($listagem);
    //$array["iTotalDisplayRecords"] = count($listagem);
    foreach ($listagem as $usuario) {
        $array["aaData"][] = array($usuario["id"], $usuario["numero"], $usuario["empresa"], $usuario["cnpj"], $usuario["telefone"], $usuario["agenda_br"], $usuario["status_chamado"]);
    }
}

echo fastjson::convert($array);
?>