-- phpMyAdmin SQL Dump
-- version 3.4.10.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 08/05/2012 às 08h50min
-- Versão do Servidor: 5.0.91
-- Versão do PHP: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `heron_chronosti`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `adm_content`
--

CREATE TABLE IF NOT EXISTS `adm_content` (
  `id` int(11) NOT NULL auto_increment,
  `content` varchar(45) NOT NULL,
  `titulo` varchar(45) NOT NULL,
  `ordenacao` int(11) default '0',
  `status` char(1) default '1',
  `datacadastro` datetime default NULL,
  `datamodificado` timestamp NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `adm_content_id` int(11) default NULL,
  `adm_content_nivel_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_adm_content_adm_content_nivel1` (`adm_content_nivel_id`),
  KEY `fk_adm_content_adm_content1` (`adm_content_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=38 ;

--
-- Extraindo dados da tabela `adm_content`
--

INSERT INTO `adm_content` (`id`, `content`, `titulo`, `ordenacao`, `status`, `datacadastro`, `datamodificado`, `adm_content_id`, `adm_content_nivel_id`) VALUES
(1, 'usuarios', 'Usuários', 1, '1', '2012-03-07 11:37:15', '2011-12-17 13:37:07', NULL, 2),
(2, 'chamados', 'Chamados', 2, '1', '2012-03-07 11:37:15', '2012-03-07 14:40:37', NULL, 5),
(8, 'usuarios/editar', 'Usuários - Editar', 1, '1', '2012-03-07 11:37:15', '2011-12-17 13:37:08', 1, 2),
(11, 'usuarios/remover', 'Usuários - Remover', 1, '1', '2012-03-07 11:37:15', '2011-12-17 13:37:09', 1, 2),
(15, 'chamados/editar', 'Chamados - Editar', 2, '1', '2012-03-07 11:37:15', '2012-03-07 16:36:15', 2, 5),
(20, 'chamados/remover', 'Chamados - Remover', 2, '1', '2012-03-07 11:37:15', '2012-03-07 14:41:02', 2, 4),
(35, 'chamados/fechar', 'Chamados - Fechar', 2, '1', '2012-03-07 11:39:42', '2012-03-07 14:40:59', 2, 4),
(36, 'chamados/abrir', 'Chamados - Abrir', 2, '1', '2012-03-07 13:36:02', '2012-03-07 16:36:02', 2, 4),
(37, 'conta/editar', 'Minha Conta', 0, '1', '2012-03-09 06:07:30', '2012-03-09 09:07:30', NULL, 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `adm_content_nivel`
--

CREATE TABLE IF NOT EXISTS `adm_content_nivel` (
  `id` int(11) NOT NULL auto_increment,
  `titulo` varchar(45) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `adm_content_nivel`
--

INSERT INTO `adm_content_nivel` (`id`, `titulo`) VALUES
(1, 'Super-Usuário'),
(2, 'Master'),
(3, 'Administrador'),
(4, 'Técnico'),
(5, 'Operador');

-- --------------------------------------------------------

--
-- Estrutura stand-in para visualizar `adm_content_view`
--
CREATE TABLE IF NOT EXISTS `adm_content_view` (
`id` int(11)
,`content` varchar(45)
,`titulo` varchar(45)
,`ordenacao` int(11)
,`status` char(1)
,`datacadastro` datetime
,`datamodificado` timestamp
,`adm_content_id` int(11)
,`adm_content_nivel_id` int(11)
,`datacadastro_br` varchar(25)
,`datamodificado_br` varchar(25)
,`adm_content_nivel_titulo` varchar(45)
);
-- --------------------------------------------------------

--
-- Estrutura da tabela `bancos`
--

CREATE TABLE IF NOT EXISTS `bancos` (
  `id` int(11) NOT NULL auto_increment,
  `banco` varchar(45) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `bancos`
--

INSERT INTO `bancos` (`id`, `banco`) VALUES
(1, 'Citibank'),
(2, 'Santander'),
(3, 'HSBC'),
(4, 'ATP-CEF'),
(5, 'Neogrid'),
(6, 'Corban');

-- --------------------------------------------------------

--
-- Estrutura da tabela `chamados`
--

CREATE TABLE IF NOT EXISTS `chamados` (
  `id` int(11) NOT NULL auto_increment,
  `numero` char(8) default NULL,
  `empresa` varchar(255) NOT NULL,
  `cnpj` varchar(20) NOT NULL,
  `telefone` varchar(15) NOT NULL,
  `endereco_logradouro` varchar(255) NOT NULL,
  `endereco_complemento` varchar(50) default NULL,
  `endereco_bairro` varchar(255) NOT NULL,
  `endereco_cidade` varchar(255) NOT NULL,
  `endereco_estado` varchar(2) NOT NULL,
  `endereco_cep` varchar(15) default NULL,
  `anotacoes` mediumtext,
  `agenda_data` date NOT NULL,
  `agenda_hora_ini` time NOT NULL,
  `agenda_hora_fim` time NOT NULL,
  `atendimento_data` date default NULL,
  `atendimento_hora_ini` time default NULL,
  `atendimento_hora_fim` time default NULL,
  `visita_status` char(1) default NULL,
  `tecnico` varchar(255) default NULL,
  `observacao` mediumtext,
  `status` char(1) default '1',
  `datacadastro` datetime default NULL,
  `datamodificado` timestamp NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `bancos_id` int(11) NOT NULL,
  `usuarios_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_chamados_bancos1` (`bancos_id`),
  KEY `fk_chamados_usuarios1` (`usuarios_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `chamados`
--

INSERT INTO `chamados` (`id`, `numero`, `empresa`, `cnpj`, `telefone`, `endereco_logradouro`, `endereco_complemento`, `endereco_bairro`, `endereco_cidade`, `endereco_estado`, `endereco_cep`, `anotacoes`, `agenda_data`, `agenda_hora_ini`, `agenda_hora_fim`, `atendimento_data`, `atendimento_hora_ini`, `atendimento_hora_fim`, `visita_status`, `tecnico`, `observacao`, `status`, `datacadastro`, `datamodificado`, `bancos_id`, `usuarios_id`) VALUES
(1, 'S0000001', 'Insign Digital', '34.294.802/9348-09', '(21) 9264-5688', 'R Luis Tomas, 582', NULL, 'Luz', 'Nova Iguaçu', 'RJ', '26256-100', 'Efetuar os procedimentos!', '2012-03-20', '12:00:00', '16:00:00', '2012-12-12', '18:34:00', '19:40:00', '1', 'Zé Polvilho', 'Procedimentos efetuados com sucesso! 123123', '0', '2012-03-08 18:33:32', '2012-03-09 13:00:49', 2, 1),
(2, 'S0000002', 'Albatroz Ltda', '12.354.572/0001-55', '(21) 2157-6654', 'Av São João n.885', NULL, 'Centro', 'Nova Iguaçu', 'RJ', '26000-000', 'Contato com o Sr. Denilson', '2012-03-10', '09:00:00', '17:00:00', '2012-03-10', '09:30:00', '17:00:00', '3', 'Junior', 'Visita cancelada pelo cliente', '2', '2012-03-09 09:54:30', '2012-03-09 08:16:09', 1, 2),
(3, 'S0000003', 'Universidade Iguaçu', '12.542.515/0001-56', '(21) 2465-4524', 'Avenida Abilio Tavaro n.° 856', NULL, 'Bairro da Luz', 'Nova Iguaçu', 'RJ', '26000-000', 'Atender pela parte da manhã', '2012-03-13', '09:00:00', '12:00:00', NULL, NULL, NULL, NULL, NULL, NULL, '1', '2012-03-13 12:19:37', '2012-03-13 11:19:37', 1, 2);

-- --------------------------------------------------------

--
-- Estrutura stand-in para visualizar `chamados_produtos_view`
--
CREATE TABLE IF NOT EXISTS `chamados_produtos_view` (
`produtos_id` int(11)
,`chamados_id` int(11)
,`produto` varchar(45)
);
-- --------------------------------------------------------

--
-- Estrutura stand-in para visualizar `chamados_servicos_view`
--
CREATE TABLE IF NOT EXISTS `chamados_servicos_view` (
`servicos_id` int(11)
,`chamados_id` int(11)
,`servico` varchar(45)
);
-- --------------------------------------------------------

--
-- Estrutura stand-in para visualizar `chamados_view`
--
CREATE TABLE IF NOT EXISTS `chamados_view` (
`id` int(11)
,`numero` char(8)
,`empresa` varchar(255)
,`cnpj` varchar(20)
,`telefone` varchar(15)
,`endereco_logradouro` varchar(255)
,`endereco_complemento` varchar(50)
,`endereco_bairro` varchar(255)
,`endereco_cidade` varchar(255)
,`endereco_estado` varchar(2)
,`endereco_cep` varchar(15)
,`anotacoes` mediumtext
,`agenda_data` date
,`agenda_hora_ini` time
,`agenda_hora_fim` time
,`atendimento_data` date
,`atendimento_hora_ini` time
,`atendimento_hora_fim` time
,`visita_status` char(1)
,`tecnico` varchar(255)
,`observacao` mediumtext
,`status` char(1)
,`datacadastro` datetime
,`datamodificado` timestamp
,`bancos_id` int(11)
,`usuarios_id` int(11)
,`agenda` varchar(10)
,`atendimento` varchar(10)
,`agenda_br` varchar(32)
,`atendimento_br` varchar(32)
,`datacadastro_br` varchar(25)
,`datamodificado_br` varchar(25)
,`banco` varchar(45)
,`usuarios_nome` varchar(255)
,`status_chamado` varchar(7)
);
-- --------------------------------------------------------

--
-- Estrutura da tabela `historico`
--

CREATE TABLE IF NOT EXISTS `historico` (
  `id` int(11) NOT NULL auto_increment,
  `acao` varchar(255) NOT NULL,
  `server_variables` mediumtext NOT NULL,
  `request_variables` mediumtext,
  `session_variables` mediumtext,
  `cookie_variables` mediumtext NOT NULL,
  `data` timestamp NULL default CURRENT_TIMESTAMP,
  `usuarios_id` int(11) default NULL,
  `adm_content_id` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_historico_usuarios1` (`usuarios_id`),
  KEY `fk_historico_adm_content1` (`adm_content_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Extraindo dados da tabela `historico`
--

INSERT INTO `historico` (`id`, `acao`, `server_variables`, `request_variables`, `session_variables`, `cookie_variables`, `data`, `usuarios_id`, `adm_content_id`) VALUES
(1, 'Problemas ao efetuar login.', 'a:37:{s:14:"CONTENT_LENGTH";s:2:"32";s:12:"CONTENT_TYPE";s:33:"application/x-www-form-urlencoded";s:13:"DOCUMENT_ROOT";s:24:"/home2/heron/public_html";s:17:"GATEWAY_INTERFACE";s:7:"CGI/1.1";s:11:"HTTP_ACCEPT";s:129:"text/html, application/xml;q=0.9, application/xhtml+xml, image/png, image/webp, image/jpeg, image/gif, image/x-xbitmap, */*;q=0.1";s:20:"HTTP_ACCEPT_ENCODING";s:13:"gzip, deflate";s:20:"HTTP_ACCEPT_LANGUAGE";s:23:"pt-BR,pt;q=0.9,en;q=0.8";s:15:"HTTP_CONNECTION";s:10:"Keep-Alive";s:11:"HTTP_COOKIE";s:184:"PHPSESSID=9471497ee4c27c32d385f018aebb4ae2; __utma=267039964.876309040.1334186112.1334186112.1334186112.1; __utmz=267039964.1334186112.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";s:9:"HTTP_HOST";s:21:"www.chronos-ti.com.br";s:12:"HTTP_REFERER";s:45:"http://www.chronos-ti.com.br/admin/index.html";s:15:"HTTP_USER_AGENT";s:67:"Opera/9.80 (Windows NT 6.1; U; pt-BR) Presto/2.10.229 Version/11.62";s:4:"PATH";s:13:"/bin:/usr/bin";s:16:"PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:12:"QUERY_STRING";s:0:"";s:25:"REDIRECT_PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:15:"REDIRECT_STATUS";s:3:"200";s:18:"REDIRECT_UNIQUE_ID";s:24:"T4pPNbtswBAAAAvnXGsAAABp";s:12:"REDIRECT_URL";s:17:"/admin/index.html";s:11:"REMOTE_ADDR";s:14:"187.122.137.21";s:11:"REMOTE_PORT";s:5:"57403";s:14:"REQUEST_METHOD";s:4:"POST";s:11:"REQUEST_URI";s:17:"/admin/index.html";s:15:"SCRIPT_FILENAME";s:34:"/home2/heron/public_html/index.php";s:11:"SCRIPT_NAME";s:10:"/index.php";s:11:"SERVER_ADDR";s:14:"187.108.192.16";s:12:"SERVER_ADMIN";s:23:"webmaster@kengoo.com.br";s:11:"SERVER_NAME";s:21:"www.chronos-ti.com.br";s:11:"SERVER_PORT";s:2:"80";s:15:"SERVER_PROTOCOL";s:8:"HTTP/1.1";s:16:"SERVER_SIGNATURE";s:0:"";s:15:"SERVER_SOFTWARE";s:6:"Apache";s:9:"UNIQUE_ID";s:24:"T4pPNbtswBAAAAvnXGsAAABp";s:8:"PHP_SELF";s:10:"/index.php";s:12:"REQUEST_TIME";i:1334464309;s:4:"argv";a:0:{}s:4:"argc";i:0;}', 'a:5:{s:8:"username";s:5:"heron";s:8:"password";s:8:"skar2006";s:9:"PHPSESSID";s:32:"9471497ee4c27c32d385f018aebb4ae2";s:6:"__utma";s:54:"267039964.876309040.1334186112.1334186112.1334186112.1";s:6:"__utmz";s:70:"267039964.1334186112.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', 'a:0:{}', 'a:3:{s:9:"PHPSESSID";s:32:"9471497ee4c27c32d385f018aebb4ae2";s:6:"__utma";s:54:"267039964.876309040.1334186112.1334186112.1334186112.1";s:6:"__utmz";s:70:"267039964.1334186112.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', '2012-04-15 04:31:49', NULL, NULL),
(2, 'Problemas ao efetuar login.', 'a:36:{s:14:"CONTENT_LENGTH";s:2:"32";s:12:"CONTENT_TYPE";s:33:"application/x-www-form-urlencoded";s:13:"DOCUMENT_ROOT";s:24:"/home2/heron/public_html";s:17:"GATEWAY_INTERFACE";s:7:"CGI/1.1";s:11:"HTTP_ACCEPT";s:129:"text/html, application/xml;q=0.9, application/xhtml+xml, image/png, image/webp, image/jpeg, image/gif, image/x-xbitmap, */*;q=0.1";s:20:"HTTP_ACCEPT_ENCODING";s:13:"gzip, deflate";s:20:"HTTP_ACCEPT_LANGUAGE";s:23:"pt-BR,pt;q=0.9,en;q=0.8";s:15:"HTTP_CONNECTION";s:10:"Keep-Alive";s:11:"HTTP_COOKIE";s:184:"PHPSESSID=9471497ee4c27c32d385f018aebb4ae2; __utma=267039964.876309040.1334186112.1334186112.1334186112.1; __utmz=267039964.1334186112.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";s:9:"HTTP_HOST";s:21:"www.chronos-ti.com.br";s:15:"HTTP_USER_AGENT";s:67:"Opera/9.80 (Windows NT 6.1; U; pt-BR) Presto/2.10.229 Version/11.62";s:4:"PATH";s:13:"/bin:/usr/bin";s:16:"PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:12:"QUERY_STRING";s:0:"";s:25:"REDIRECT_PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:15:"REDIRECT_STATUS";s:3:"200";s:18:"REDIRECT_UNIQUE_ID";s:24:"T4pQHrtswBAAAATg@tkAAAAB";s:12:"REDIRECT_URL";s:17:"/admin/index.html";s:11:"REMOTE_ADDR";s:14:"187.122.137.21";s:11:"REMOTE_PORT";s:5:"57421";s:14:"REQUEST_METHOD";s:4:"POST";s:11:"REQUEST_URI";s:17:"/admin/index.html";s:15:"SCRIPT_FILENAME";s:34:"/home2/heron/public_html/index.php";s:11:"SCRIPT_NAME";s:10:"/index.php";s:11:"SERVER_ADDR";s:14:"187.108.192.16";s:12:"SERVER_ADMIN";s:23:"webmaster@kengoo.com.br";s:11:"SERVER_NAME";s:21:"www.chronos-ti.com.br";s:11:"SERVER_PORT";s:2:"80";s:15:"SERVER_PROTOCOL";s:8:"HTTP/1.1";s:16:"SERVER_SIGNATURE";s:0:"";s:15:"SERVER_SOFTWARE";s:6:"Apache";s:9:"UNIQUE_ID";s:24:"T4pQHrtswBAAAATg@tkAAAAB";s:8:"PHP_SELF";s:10:"/index.php";s:12:"REQUEST_TIME";i:1334464542;s:4:"argv";a:0:{}s:4:"argc";i:0;}', 'a:5:{s:8:"username";s:5:"heron";s:8:"password";s:8:"skar2006";s:9:"PHPSESSID";s:32:"9471497ee4c27c32d385f018aebb4ae2";s:6:"__utma";s:54:"267039964.876309040.1334186112.1334186112.1334186112.1";s:6:"__utmz";s:70:"267039964.1334186112.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', 'a:0:{}', 'a:3:{s:9:"PHPSESSID";s:32:"9471497ee4c27c32d385f018aebb4ae2";s:6:"__utma";s:54:"267039964.876309040.1334186112.1334186112.1334186112.1";s:6:"__utmz";s:70:"267039964.1334186112.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', '2012-04-15 04:35:42', NULL, NULL),
(3, 'Problemas ao efetuar login.', 'a:36:{s:14:"CONTENT_LENGTH";s:2:"32";s:12:"CONTENT_TYPE";s:33:"application/x-www-form-urlencoded";s:13:"DOCUMENT_ROOT";s:24:"/home2/heron/public_html";s:17:"GATEWAY_INTERFACE";s:7:"CGI/1.1";s:11:"HTTP_ACCEPT";s:129:"text/html, application/xml;q=0.9, application/xhtml+xml, image/png, image/webp, image/jpeg, image/gif, image/x-xbitmap, */*;q=0.1";s:20:"HTTP_ACCEPT_ENCODING";s:13:"gzip, deflate";s:20:"HTTP_ACCEPT_LANGUAGE";s:23:"pt-BR,pt;q=0.9,en;q=0.8";s:15:"HTTP_CONNECTION";s:10:"Keep-Alive";s:11:"HTTP_COOKIE";s:184:"PHPSESSID=9471497ee4c27c32d385f018aebb4ae2; __utma=267039964.876309040.1334186112.1334186112.1334186112.1; __utmz=267039964.1334186112.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";s:9:"HTTP_HOST";s:21:"www.chronos-ti.com.br";s:15:"HTTP_USER_AGENT";s:67:"Opera/9.80 (Windows NT 6.1; U; pt-BR) Presto/2.10.229 Version/11.62";s:4:"PATH";s:13:"/bin:/usr/bin";s:16:"PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:12:"QUERY_STRING";s:0:"";s:25:"REDIRECT_PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:15:"REDIRECT_STATUS";s:3:"200";s:18:"REDIRECT_UNIQUE_ID";s:24:"T4pQZ7tswBAAAHCQ18wAAAAu";s:12:"REDIRECT_URL";s:17:"/admin/index.html";s:11:"REMOTE_ADDR";s:14:"187.122.137.21";s:11:"REMOTE_PORT";s:5:"57437";s:14:"REQUEST_METHOD";s:4:"POST";s:11:"REQUEST_URI";s:17:"/admin/index.html";s:15:"SCRIPT_FILENAME";s:34:"/home2/heron/public_html/index.php";s:11:"SCRIPT_NAME";s:10:"/index.php";s:11:"SERVER_ADDR";s:14:"187.108.192.16";s:12:"SERVER_ADMIN";s:23:"webmaster@kengoo.com.br";s:11:"SERVER_NAME";s:21:"www.chronos-ti.com.br";s:11:"SERVER_PORT";s:2:"80";s:15:"SERVER_PROTOCOL";s:8:"HTTP/1.1";s:16:"SERVER_SIGNATURE";s:0:"";s:15:"SERVER_SOFTWARE";s:6:"Apache";s:9:"UNIQUE_ID";s:24:"T4pQZ7tswBAAAHCQ18wAAAAu";s:8:"PHP_SELF";s:10:"/index.php";s:12:"REQUEST_TIME";i:1334464615;s:4:"argv";a:0:{}s:4:"argc";i:0;}', 'a:5:{s:8:"username";s:5:"heron";s:8:"password";s:8:"skar2006";s:9:"PHPSESSID";s:32:"9471497ee4c27c32d385f018aebb4ae2";s:6:"__utma";s:54:"267039964.876309040.1334186112.1334186112.1334186112.1";s:6:"__utmz";s:70:"267039964.1334186112.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', 'a:0:{}', 'a:3:{s:9:"PHPSESSID";s:32:"9471497ee4c27c32d385f018aebb4ae2";s:6:"__utma";s:54:"267039964.876309040.1334186112.1334186112.1334186112.1";s:6:"__utmz";s:70:"267039964.1334186112.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', '2012-04-15 04:36:55', NULL, NULL),
(4, 'Problemas ao efetuar login.', 'a:37:{s:14:"CONTENT_LENGTH";s:2:"32";s:12:"CONTENT_TYPE";s:33:"application/x-www-form-urlencoded";s:13:"DOCUMENT_ROOT";s:24:"/home2/heron/public_html";s:17:"GATEWAY_INTERFACE";s:7:"CGI/1.1";s:11:"HTTP_ACCEPT";s:129:"text/html, application/xml;q=0.9, application/xhtml+xml, image/png, image/webp, image/jpeg, image/gif, image/x-xbitmap, */*;q=0.1";s:20:"HTTP_ACCEPT_ENCODING";s:13:"gzip, deflate";s:20:"HTTP_ACCEPT_LANGUAGE";s:23:"pt-BR,pt;q=0.9,en;q=0.8";s:15:"HTTP_CONNECTION";s:10:"Keep-Alive";s:11:"HTTP_COOKIE";s:184:"PHPSESSID=9471497ee4c27c32d385f018aebb4ae2; __utma=267039964.876309040.1334186112.1334186112.1334186112.1; __utmz=267039964.1334186112.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";s:9:"HTTP_HOST";s:21:"www.chronos-ti.com.br";s:12:"HTTP_REFERER";s:45:"http://www.chronos-ti.com.br/admin/index.html";s:15:"HTTP_USER_AGENT";s:67:"Opera/9.80 (Windows NT 6.1; U; pt-BR) Presto/2.10.229 Version/11.62";s:4:"PATH";s:13:"/bin:/usr/bin";s:16:"PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:12:"QUERY_STRING";s:0:"";s:25:"REDIRECT_PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:15:"REDIRECT_STATUS";s:3:"200";s:18:"REDIRECT_UNIQUE_ID";s:24:"T4pQjrtswBAAAHI@aKwAAAAS";s:12:"REDIRECT_URL";s:17:"/admin/index.html";s:11:"REMOTE_ADDR";s:14:"187.122.137.21";s:11:"REMOTE_PORT";s:5:"57447";s:14:"REQUEST_METHOD";s:4:"POST";s:11:"REQUEST_URI";s:17:"/admin/index.html";s:15:"SCRIPT_FILENAME";s:34:"/home2/heron/public_html/index.php";s:11:"SCRIPT_NAME";s:10:"/index.php";s:11:"SERVER_ADDR";s:14:"187.108.192.16";s:12:"SERVER_ADMIN";s:23:"webmaster@kengoo.com.br";s:11:"SERVER_NAME";s:21:"www.chronos-ti.com.br";s:11:"SERVER_PORT";s:2:"80";s:15:"SERVER_PROTOCOL";s:8:"HTTP/1.1";s:16:"SERVER_SIGNATURE";s:0:"";s:15:"SERVER_SOFTWARE";s:6:"Apache";s:9:"UNIQUE_ID";s:24:"T4pQjrtswBAAAHI@aKwAAAAS";s:8:"PHP_SELF";s:10:"/index.php";s:12:"REQUEST_TIME";i:1334464654;s:4:"argv";a:0:{}s:4:"argc";i:0;}', 'a:5:{s:8:"username";s:5:"heron";s:8:"password";s:8:"b9295993";s:9:"PHPSESSID";s:32:"9471497ee4c27c32d385f018aebb4ae2";s:6:"__utma";s:54:"267039964.876309040.1334186112.1334186112.1334186112.1";s:6:"__utmz";s:70:"267039964.1334186112.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', 'a:0:{}', 'a:3:{s:9:"PHPSESSID";s:32:"9471497ee4c27c32d385f018aebb4ae2";s:6:"__utma";s:54:"267039964.876309040.1334186112.1334186112.1334186112.1";s:6:"__utmz";s:70:"267039964.1334186112.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', '2012-04-15 04:37:34', NULL, NULL),
(5, 'Problemas ao efetuar login.', 'a:38:{s:14:"CONTENT_LENGTH";s:2:"32";s:12:"CONTENT_TYPE";s:33:"application/x-www-form-urlencoded";s:13:"DOCUMENT_ROOT";s:24:"/home2/heron/public_html";s:17:"GATEWAY_INTERFACE";s:7:"CGI/1.1";s:11:"HTTP_ACCEPT";s:129:"text/html, application/xml;q=0.9, application/xhtml+xml, image/png, image/webp, image/jpeg, image/gif, image/x-xbitmap, */*;q=0.1";s:20:"HTTP_ACCEPT_ENCODING";s:13:"gzip, deflate";s:20:"HTTP_ACCEPT_LANGUAGE";s:23:"pt-BR,pt;q=0.9,en;q=0.8";s:18:"HTTP_CACHE_CONTROL";s:8:"no-cache";s:15:"HTTP_CONNECTION";s:10:"Keep-Alive";s:11:"HTTP_COOKIE";s:184:"PHPSESSID=9471497ee4c27c32d385f018aebb4ae2; __utma=267039964.876309040.1334186112.1334186112.1334186112.1; __utmz=267039964.1334186112.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";s:9:"HTTP_HOST";s:21:"www.chronos-ti.com.br";s:12:"HTTP_REFERER";s:45:"http://www.chronos-ti.com.br/admin/index.html";s:15:"HTTP_USER_AGENT";s:67:"Opera/9.80 (Windows NT 6.1; U; pt-BR) Presto/2.10.229 Version/11.62";s:4:"PATH";s:13:"/bin:/usr/bin";s:16:"PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:12:"QUERY_STRING";s:0:"";s:25:"REDIRECT_PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:15:"REDIRECT_STATUS";s:3:"200";s:18:"REDIRECT_UNIQUE_ID";s:24:"T4pRZ7tswBAAAATlAuwAAAAZ";s:12:"REDIRECT_URL";s:17:"/admin/index.html";s:11:"REMOTE_ADDR";s:14:"187.122.137.21";s:11:"REMOTE_PORT";s:5:"57595";s:14:"REQUEST_METHOD";s:4:"POST";s:11:"REQUEST_URI";s:17:"/admin/index.html";s:15:"SCRIPT_FILENAME";s:34:"/home2/heron/public_html/index.php";s:11:"SCRIPT_NAME";s:10:"/index.php";s:11:"SERVER_ADDR";s:14:"187.108.192.16";s:12:"SERVER_ADMIN";s:23:"webmaster@kengoo.com.br";s:11:"SERVER_NAME";s:21:"www.chronos-ti.com.br";s:11:"SERVER_PORT";s:2:"80";s:15:"SERVER_PROTOCOL";s:8:"HTTP/1.1";s:16:"SERVER_SIGNATURE";s:0:"";s:15:"SERVER_SOFTWARE";s:6:"Apache";s:9:"UNIQUE_ID";s:24:"T4pRZ7tswBAAAATlAuwAAAAZ";s:8:"PHP_SELF";s:10:"/index.php";s:12:"REQUEST_TIME";i:1334464871;s:4:"argv";a:0:{}s:4:"argc";i:0;}', 'a:5:{s:8:"username";s:5:"heron";s:8:"password";s:8:"b9295993";s:9:"PHPSESSID";s:32:"9471497ee4c27c32d385f018aebb4ae2";s:6:"__utma";s:54:"267039964.876309040.1334186112.1334186112.1334186112.1";s:6:"__utmz";s:70:"267039964.1334186112.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', 'a:0:{}', 'a:3:{s:9:"PHPSESSID";s:32:"9471497ee4c27c32d385f018aebb4ae2";s:6:"__utma";s:54:"267039964.876309040.1334186112.1334186112.1334186112.1";s:6:"__utmz";s:70:"267039964.1334186112.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', '2012-04-15 04:41:11', NULL, NULL),
(6, 'Login efetuado com sucesso.', 'a:37:{s:14:"CONTENT_LENGTH";s:2:"32";s:12:"CONTENT_TYPE";s:33:"application/x-www-form-urlencoded";s:13:"DOCUMENT_ROOT";s:24:"/home2/heron/public_html";s:17:"GATEWAY_INTERFACE";s:7:"CGI/1.1";s:11:"HTTP_ACCEPT";s:129:"text/html, application/xml;q=0.9, application/xhtml+xml, image/png, image/webp, image/jpeg, image/gif, image/x-xbitmap, */*;q=0.1";s:20:"HTTP_ACCEPT_ENCODING";s:13:"gzip, deflate";s:20:"HTTP_ACCEPT_LANGUAGE";s:23:"pt-BR,pt;q=0.9,en;q=0.8";s:15:"HTTP_CONNECTION";s:10:"Keep-Alive";s:11:"HTTP_COOKIE";s:184:"PHPSESSID=9471497ee4c27c32d385f018aebb4ae2; __utma=267039964.876309040.1334186112.1334186112.1334186112.1; __utmz=267039964.1334186112.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";s:9:"HTTP_HOST";s:21:"www.chronos-ti.com.br";s:12:"HTTP_REFERER";s:45:"http://www.chronos-ti.com.br/admin/index.html";s:15:"HTTP_USER_AGENT";s:67:"Opera/9.80 (Windows NT 6.1; U; pt-BR) Presto/2.10.229 Version/11.62";s:4:"PATH";s:13:"/bin:/usr/bin";s:16:"PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:12:"QUERY_STRING";s:0:"";s:25:"REDIRECT_PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:15:"REDIRECT_STATUS";s:3:"200";s:18:"REDIRECT_UNIQUE_ID";s:24:"T4pRartswBAAAHXHTe8AAAA1";s:12:"REDIRECT_URL";s:17:"/admin/index.html";s:11:"REMOTE_ADDR";s:14:"187.122.137.21";s:11:"REMOTE_PORT";s:5:"57605";s:14:"REQUEST_METHOD";s:4:"POST";s:11:"REQUEST_URI";s:17:"/admin/index.html";s:15:"SCRIPT_FILENAME";s:34:"/home2/heron/public_html/index.php";s:11:"SCRIPT_NAME";s:10:"/index.php";s:11:"SERVER_ADDR";s:14:"187.108.192.16";s:12:"SERVER_ADMIN";s:23:"webmaster@kengoo.com.br";s:11:"SERVER_NAME";s:21:"www.chronos-ti.com.br";s:11:"SERVER_PORT";s:2:"80";s:15:"SERVER_PROTOCOL";s:8:"HTTP/1.1";s:16:"SERVER_SIGNATURE";s:0:"";s:15:"SERVER_SOFTWARE";s:6:"Apache";s:9:"UNIQUE_ID";s:24:"T4pRartswBAAAHXHTe8AAAA1";s:8:"PHP_SELF";s:10:"/index.php";s:12:"REQUEST_TIME";i:1334464874;s:4:"argv";a:0:{}s:4:"argc";i:0;}', 'a:5:{s:8:"username";s:5:"heron";s:8:"password";s:8:"skar2006";s:9:"PHPSESSID";s:32:"9471497ee4c27c32d385f018aebb4ae2";s:6:"__utma";s:54:"267039964.876309040.1334186112.1334186112.1334186112.1";s:6:"__utmz";s:70:"267039964.1334186112.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', 'a:1:{s:7:"usuario";a:13:{s:2:"id";s:1:"1";s:7:"usuario";s:5:"heron";s:5:"senha";s:32:"d12e69c36157d034edac61831fb236fc";s:4:"nome";s:12:"Heron Santos";s:5:"email";s:22:"heronrsantos@gmail.com";s:6:"avatar";s:169:"http://www.gravatar.com/avatar/a31833a51eb26f2df5a4d5888e2946ae?size=60&amp;default=http%3A%2F%2Fwww.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fsize%3D60";s:6:"status";s:1:"1";s:12:"datacadastro";s:19:"2012-03-07 11:41:44";s:14:"datamodificado";s:19:"2012-03-09 07:49:18";s:20:"adm_content_nivel_id";s:1:"1";s:15:"datacadastro_br";s:20:"07/03/2012 às 11:41";s:17:"datamodificado_br";s:20:"09/03/2012 às 07:49";s:24:"adm_content_nivel_titulo";s:14:"Super-Usuário";}}', 'a:3:{s:9:"PHPSESSID";s:32:"9471497ee4c27c32d385f018aebb4ae2";s:6:"__utma";s:54:"267039964.876309040.1334186112.1334186112.1334186112.1";s:6:"__utmz";s:70:"267039964.1334186112.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', '2012-04-15 04:41:14', 1, NULL),
(7, 'Carregou página.', 'a:35:{s:13:"DOCUMENT_ROOT";s:24:"/home2/heron/public_html";s:17:"GATEWAY_INTERFACE";s:7:"CGI/1.1";s:11:"HTTP_ACCEPT";s:129:"text/html, application/xml;q=0.9, application/xhtml+xml, image/png, image/webp, image/jpeg, image/gif, image/x-xbitmap, */*;q=0.1";s:20:"HTTP_ACCEPT_ENCODING";s:13:"gzip, deflate";s:20:"HTTP_ACCEPT_LANGUAGE";s:23:"pt-BR,pt;q=0.9,en;q=0.8";s:15:"HTTP_CONNECTION";s:10:"Keep-Alive";s:11:"HTTP_COOKIE";s:184:"PHPSESSID=9471497ee4c27c32d385f018aebb4ae2; __utma=267039964.876309040.1334186112.1334186112.1334186112.1; __utmz=267039964.1334186112.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";s:9:"HTTP_HOST";s:21:"www.chronos-ti.com.br";s:12:"HTTP_REFERER";s:45:"http://www.chronos-ti.com.br/admin/index.html";s:15:"HTTP_USER_AGENT";s:67:"Opera/9.80 (Windows NT 6.1; U; pt-BR) Presto/2.10.229 Version/11.62";s:4:"PATH";s:13:"/bin:/usr/bin";s:16:"PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:12:"QUERY_STRING";s:0:"";s:25:"REDIRECT_PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:15:"REDIRECT_STATUS";s:3:"200";s:18:"REDIRECT_UNIQUE_ID";s:24:"T4pRa7tswBAAAAvaQYcAAABP";s:12:"REDIRECT_URL";s:18:"/admin/painel.html";s:11:"REMOTE_ADDR";s:14:"187.122.137.21";s:11:"REMOTE_PORT";s:5:"57601";s:14:"REQUEST_METHOD";s:3:"GET";s:11:"REQUEST_URI";s:18:"/admin/painel.html";s:15:"SCRIPT_FILENAME";s:34:"/home2/heron/public_html/index.php";s:11:"SCRIPT_NAME";s:10:"/index.php";s:11:"SERVER_ADDR";s:14:"187.108.192.16";s:12:"SERVER_ADMIN";s:23:"webmaster@kengoo.com.br";s:11:"SERVER_NAME";s:21:"www.chronos-ti.com.br";s:11:"SERVER_PORT";s:2:"80";s:15:"SERVER_PROTOCOL";s:8:"HTTP/1.1";s:16:"SERVER_SIGNATURE";s:0:"";s:15:"SERVER_SOFTWARE";s:6:"Apache";s:9:"UNIQUE_ID";s:24:"T4pRa7tswBAAAAvaQYcAAABP";s:8:"PHP_SELF";s:10:"/index.php";s:12:"REQUEST_TIME";i:1334464875;s:4:"argv";a:0:{}s:4:"argc";i:0;}', 'a:3:{s:9:"PHPSESSID";s:32:"9471497ee4c27c32d385f018aebb4ae2";s:6:"__utma";s:54:"267039964.876309040.1334186112.1334186112.1334186112.1";s:6:"__utmz";s:70:"267039964.1334186112.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', 'a:1:{s:7:"usuario";a:13:{s:2:"id";s:1:"1";s:7:"usuario";s:5:"heron";s:5:"senha";s:32:"d12e69c36157d034edac61831fb236fc";s:4:"nome";s:12:"Heron Santos";s:5:"email";s:22:"heronrsantos@gmail.com";s:6:"avatar";s:169:"http://www.gravatar.com/avatar/a31833a51eb26f2df5a4d5888e2946ae?size=60&amp;default=http%3A%2F%2Fwww.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fsize%3D60";s:6:"status";s:1:"1";s:12:"datacadastro";s:19:"2012-03-07 11:41:44";s:14:"datamodificado";s:19:"2012-03-09 07:49:18";s:20:"adm_content_nivel_id";s:1:"1";s:15:"datacadastro_br";s:20:"07/03/2012 às 11:41";s:17:"datamodificado_br";s:20:"09/03/2012 às 07:49";s:24:"adm_content_nivel_titulo";s:14:"Super-Usuário";}}', 'a:3:{s:9:"PHPSESSID";s:32:"9471497ee4c27c32d385f018aebb4ae2";s:6:"__utma";s:54:"267039964.876309040.1334186112.1334186112.1334186112.1";s:6:"__utmz";s:70:"267039964.1334186112.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', '2012-04-15 04:41:15', 1, NULL),
(8, 'Carregou página.', 'a:35:{s:13:"DOCUMENT_ROOT";s:24:"/home2/heron/public_html";s:17:"GATEWAY_INTERFACE";s:7:"CGI/1.1";s:11:"HTTP_ACCEPT";s:129:"text/html, application/xml;q=0.9, application/xhtml+xml, image/png, image/webp, image/jpeg, image/gif, image/x-xbitmap, */*;q=0.1";s:20:"HTTP_ACCEPT_ENCODING";s:13:"gzip, deflate";s:20:"HTTP_ACCEPT_LANGUAGE";s:23:"pt-BR,pt;q=0.9,en;q=0.8";s:15:"HTTP_CONNECTION";s:10:"Keep-Alive";s:11:"HTTP_COOKIE";s:184:"PHPSESSID=9471497ee4c27c32d385f018aebb4ae2; __utma=267039964.876309040.1334186112.1334186112.1334186112.1; __utmz=267039964.1334186112.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";s:9:"HTTP_HOST";s:21:"www.chronos-ti.com.br";s:15:"HTTP_USER_AGENT";s:67:"Opera/9.80 (Windows NT 6.1; U; pt-BR) Presto/2.10.229 Version/11.62";s:4:"PATH";s:13:"/bin:/usr/bin";s:16:"PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:12:"QUERY_STRING";s:16:"content=chamados";s:25:"REDIRECT_PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:21:"REDIRECT_QUERY_STRING";s:16:"content=chamados";s:15:"REDIRECT_STATUS";s:3:"200";s:18:"REDIRECT_UNIQUE_ID";s:24:"T4pRcrtswBAAAAvaQYoAAABP";s:12:"REDIRECT_URL";s:18:"/admin/painel.html";s:11:"REMOTE_ADDR";s:14:"187.122.137.21";s:11:"REMOTE_PORT";s:5:"57601";s:14:"REQUEST_METHOD";s:3:"GET";s:11:"REQUEST_URI";s:35:"/admin/painel.html?content=chamados";s:15:"SCRIPT_FILENAME";s:34:"/home2/heron/public_html/index.php";s:11:"SCRIPT_NAME";s:10:"/index.php";s:11:"SERVER_ADDR";s:14:"187.108.192.16";s:12:"SERVER_ADMIN";s:23:"webmaster@kengoo.com.br";s:11:"SERVER_NAME";s:21:"www.chronos-ti.com.br";s:11:"SERVER_PORT";s:2:"80";s:15:"SERVER_PROTOCOL";s:8:"HTTP/1.1";s:16:"SERVER_SIGNATURE";s:0:"";s:15:"SERVER_SOFTWARE";s:6:"Apache";s:9:"UNIQUE_ID";s:24:"T4pRcrtswBAAAAvaQYoAAABP";s:8:"PHP_SELF";s:10:"/index.php";s:12:"REQUEST_TIME";i:1334464882;s:4:"argv";a:1:{i:0;s:16:"content=chamados";}s:4:"argc";i:1;}', 'a:4:{s:7:"content";s:8:"chamados";s:9:"PHPSESSID";s:32:"9471497ee4c27c32d385f018aebb4ae2";s:6:"__utma";s:54:"267039964.876309040.1334186112.1334186112.1334186112.1";s:6:"__utmz";s:70:"267039964.1334186112.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', 'a:2:{s:7:"usuario";a:13:{s:2:"id";s:1:"1";s:7:"usuario";s:5:"heron";s:5:"senha";s:32:"d12e69c36157d034edac61831fb236fc";s:4:"nome";s:12:"Heron Santos";s:5:"email";s:22:"heronrsantos@gmail.com";s:6:"avatar";s:169:"http://www.gravatar.com/avatar/a31833a51eb26f2df5a4d5888e2946ae?size=60&amp;default=http%3A%2F%2Fwww.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fsize%3D60";s:6:"status";s:1:"1";s:12:"datacadastro";s:19:"2012-03-07 11:41:44";s:14:"datamodificado";s:19:"2012-03-09 07:49:18";s:20:"adm_content_nivel_id";s:1:"1";s:15:"datacadastro_br";s:20:"07/03/2012 às 11:41";s:17:"datamodificado_br";s:20:"09/03/2012 às 07:49";s:24:"adm_content_nivel_titulo";s:14:"Super-Usuário";}s:11:"adm_content";a:12:{s:2:"id";s:1:"2";s:7:"content";s:8:"chamados";s:6:"titulo";s:8:"Chamados";s:9:"ordenacao";s:1:"2";s:6:"status";s:1:"1";s:12:"datacadastro";s:19:"2012-03-07 11:37:15";s:14:"datamodificado";s:19:"2012-03-07 11:40:37";s:14:"adm_content_id";N;s:20:"adm_content_nivel_id";s:1:"5";s:15:"datacadastro_br";s:20:"07/03/2012 às 11:37";s:17:"datamodificado_br";s:20:"07/03/2012 às 11:40";s:24:"adm_content_nivel_titulo";s:8:"Operador";}}', 'a:3:{s:9:"PHPSESSID";s:32:"9471497ee4c27c32d385f018aebb4ae2";s:6:"__utma";s:54:"267039964.876309040.1334186112.1334186112.1334186112.1";s:6:"__utmz";s:70:"267039964.1334186112.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', '2012-04-15 04:41:22', 1, 2),
(9, 'Problemas ao efetuar login.', 'a:37:{s:14:"CONTENT_LENGTH";s:2:"39";s:12:"CONTENT_TYPE";s:33:"application/x-www-form-urlencoded";s:13:"DOCUMENT_ROOT";s:24:"/home2/heron/public_html";s:17:"GATEWAY_INTERFACE";s:7:"CGI/1.1";s:11:"HTTP_ACCEPT";s:63:"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";s:20:"HTTP_ACCEPT_ENCODING";s:13:"gzip, deflate";s:20:"HTTP_ACCEPT_LANGUAGE";s:35:"pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3";s:15:"HTTP_CONNECTION";s:10:"keep-alive";s:11:"HTTP_COOKIE";s:237:"PHPSESSID=a6afe8e13507ff74c23c8a41b182c8f4; __utma=267039964.1054841183.1335450194.1335450194.1335450194.1; __utmb=267039964.1.10.1335450194; __utmc=267039964; __utmz=267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";s:9:"HTTP_HOST";s:21:"www.chronos-ti.com.br";s:12:"HTTP_REFERER";s:45:"http://www.chronos-ti.com.br/admin/index.html";s:15:"HTTP_USER_AGENT";s:65:"Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0";s:4:"PATH";s:13:"/bin:/usr/bin";s:16:"PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:12:"QUERY_STRING";s:0:"";s:25:"REDIRECT_PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:15:"REDIRECT_STATUS";s:3:"200";s:18:"REDIRECT_UNIQUE_ID";s:24:"T5lbp7tswBAAAFXrTtIAAAD5";s:12:"REDIRECT_URL";s:17:"/admin/index.html";s:11:"REMOTE_ADDR";s:12:"157.86.65.93";s:11:"REMOTE_PORT";s:5:"52841";s:14:"REQUEST_METHOD";s:4:"POST";s:11:"REQUEST_URI";s:17:"/admin/index.html";s:15:"SCRIPT_FILENAME";s:34:"/home2/heron/public_html/index.php";s:11:"SCRIPT_NAME";s:10:"/index.php";s:11:"SERVER_ADDR";s:14:"187.108.192.16";s:12:"SERVER_ADMIN";s:23:"webmaster@kengoo.com.br";s:11:"SERVER_NAME";s:21:"www.chronos-ti.com.br";s:11:"SERVER_PORT";s:2:"80";s:15:"SERVER_PROTOCOL";s:8:"HTTP/1.1";s:16:"SERVER_SIGNATURE";s:0:"";s:15:"SERVER_SOFTWARE";s:6:"Apache";s:9:"UNIQUE_ID";s:24:"T5lbp7tswBAAAFXrTtIAAAD5";s:8:"PHP_SELF";s:10:"/index.php";s:12:"REQUEST_TIME";i:1335450535;s:4:"argv";a:0:{}s:4:"argc";i:0;}', 'a:7:{s:8:"username";s:12:"heronrsantos";s:8:"password";s:8:"b9295993";s:9:"PHPSESSID";s:32:"a6afe8e13507ff74c23c8a41b182c8f4";s:6:"__utma";s:55:"267039964.1054841183.1335450194.1335450194.1335450194.1";s:6:"__utmb";s:25:"267039964.1.10.1335450194";s:6:"__utmc";s:9:"267039964";s:6:"__utmz";s:70:"267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', 'a:0:{}', 'a:5:{s:9:"PHPSESSID";s:32:"a6afe8e13507ff74c23c8a41b182c8f4";s:6:"__utma";s:55:"267039964.1054841183.1335450194.1335450194.1335450194.1";s:6:"__utmb";s:25:"267039964.1.10.1335450194";s:6:"__utmc";s:9:"267039964";s:6:"__utmz";s:70:"267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', '2012-04-26 14:28:56', NULL, NULL),
(10, 'Problemas ao efetuar login.', 'a:37:{s:14:"CONTENT_LENGTH";s:2:"39";s:12:"CONTENT_TYPE";s:33:"application/x-www-form-urlencoded";s:13:"DOCUMENT_ROOT";s:24:"/home2/heron/public_html";s:17:"GATEWAY_INTERFACE";s:7:"CGI/1.1";s:11:"HTTP_ACCEPT";s:63:"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";s:20:"HTTP_ACCEPT_ENCODING";s:13:"gzip, deflate";s:20:"HTTP_ACCEPT_LANGUAGE";s:35:"pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3";s:15:"HTTP_CONNECTION";s:10:"keep-alive";s:11:"HTTP_COOKIE";s:237:"PHPSESSID=a6afe8e13507ff74c23c8a41b182c8f4; __utma=267039964.1054841183.1335450194.1335450194.1335450194.1; __utmb=267039964.1.10.1335450194; __utmc=267039964; __utmz=267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";s:9:"HTTP_HOST";s:21:"www.chronos-ti.com.br";s:12:"HTTP_REFERER";s:45:"http://www.chronos-ti.com.br/admin/index.html";s:15:"HTTP_USER_AGENT";s:65:"Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0";s:4:"PATH";s:13:"/bin:/usr/bin";s:16:"PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:12:"QUERY_STRING";s:0:"";s:25:"REDIRECT_PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:15:"REDIRECT_STATUS";s:3:"200";s:18:"REDIRECT_UNIQUE_ID";s:24:"T5lcA7tswBAAAEcKmCEAAAFG";s:12:"REDIRECT_URL";s:17:"/admin/index.html";s:11:"REMOTE_ADDR";s:12:"157.86.65.93";s:11:"REMOTE_PORT";s:5:"52873";s:14:"REQUEST_METHOD";s:4:"POST";s:11:"REQUEST_URI";s:17:"/admin/index.html";s:15:"SCRIPT_FILENAME";s:34:"/home2/heron/public_html/index.php";s:11:"SCRIPT_NAME";s:10:"/index.php";s:11:"SERVER_ADDR";s:14:"187.108.192.16";s:12:"SERVER_ADMIN";s:23:"webmaster@kengoo.com.br";s:11:"SERVER_NAME";s:21:"www.chronos-ti.com.br";s:11:"SERVER_PORT";s:2:"80";s:15:"SERVER_PROTOCOL";s:8:"HTTP/1.1";s:16:"SERVER_SIGNATURE";s:0:"";s:15:"SERVER_SOFTWARE";s:6:"Apache";s:9:"UNIQUE_ID";s:24:"T5lcA7tswBAAAEcKmCEAAAFG";s:8:"PHP_SELF";s:10:"/index.php";s:12:"REQUEST_TIME";i:1335450627;s:4:"argv";a:0:{}s:4:"argc";i:0;}', 'a:7:{s:8:"username";s:12:"heronrsantos";s:8:"password";s:8:"skar2006";s:9:"PHPSESSID";s:32:"a6afe8e13507ff74c23c8a41b182c8f4";s:6:"__utma";s:55:"267039964.1054841183.1335450194.1335450194.1335450194.1";s:6:"__utmb";s:25:"267039964.1.10.1335450194";s:6:"__utmc";s:9:"267039964";s:6:"__utmz";s:70:"267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', 'a:0:{}', 'a:5:{s:9:"PHPSESSID";s:32:"a6afe8e13507ff74c23c8a41b182c8f4";s:6:"__utma";s:55:"267039964.1054841183.1335450194.1335450194.1335450194.1";s:6:"__utmb";s:25:"267039964.1.10.1335450194";s:6:"__utmc";s:9:"267039964";s:6:"__utmz";s:70:"267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', '2012-04-26 14:30:36', NULL, NULL),
(11, 'Login efetuado com sucesso.', 'a:37:{s:14:"CONTENT_LENGTH";s:2:"32";s:12:"CONTENT_TYPE";s:33:"application/x-www-form-urlencoded";s:13:"DOCUMENT_ROOT";s:24:"/home2/heron/public_html";s:17:"GATEWAY_INTERFACE";s:7:"CGI/1.1";s:11:"HTTP_ACCEPT";s:63:"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";s:20:"HTTP_ACCEPT_ENCODING";s:13:"gzip, deflate";s:20:"HTTP_ACCEPT_LANGUAGE";s:35:"pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3";s:15:"HTTP_CONNECTION";s:10:"keep-alive";s:11:"HTTP_COOKIE";s:237:"PHPSESSID=a6afe8e13507ff74c23c8a41b182c8f4; __utma=267039964.1054841183.1335450194.1335450194.1335450194.1; __utmb=267039964.1.10.1335450194; __utmc=267039964; __utmz=267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";s:9:"HTTP_HOST";s:21:"www.chronos-ti.com.br";s:12:"HTTP_REFERER";s:45:"http://www.chronos-ti.com.br/admin/index.html";s:15:"HTTP_USER_AGENT";s:65:"Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0";s:4:"PATH";s:13:"/bin:/usr/bin";s:16:"PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:12:"QUERY_STRING";s:0:"";s:25:"REDIRECT_PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:15:"REDIRECT_STATUS";s:3:"200";s:18:"REDIRECT_UNIQUE_ID";s:24:"T5lckbtswBAAAH43aKEAAABp";s:12:"REDIRECT_URL";s:17:"/admin/index.html";s:11:"REMOTE_ADDR";s:12:"157.86.65.93";s:11:"REMOTE_PORT";s:5:"52910";s:14:"REQUEST_METHOD";s:4:"POST";s:11:"REQUEST_URI";s:17:"/admin/index.html";s:15:"SCRIPT_FILENAME";s:34:"/home2/heron/public_html/index.php";s:11:"SCRIPT_NAME";s:10:"/index.php";s:11:"SERVER_ADDR";s:14:"187.108.192.16";s:12:"SERVER_ADMIN";s:23:"webmaster@kengoo.com.br";s:11:"SERVER_NAME";s:21:"www.chronos-ti.com.br";s:11:"SERVER_PORT";s:2:"80";s:15:"SERVER_PROTOCOL";s:8:"HTTP/1.1";s:16:"SERVER_SIGNATURE";s:0:"";s:15:"SERVER_SOFTWARE";s:6:"Apache";s:9:"UNIQUE_ID";s:24:"T5lckbtswBAAAH43aKEAAABp";s:8:"PHP_SELF";s:10:"/index.php";s:12:"REQUEST_TIME";i:1335450769;s:4:"argv";a:0:{}s:4:"argc";i:0;}', 'a:7:{s:8:"username";s:5:"heron";s:8:"password";s:8:"skar2006";s:9:"PHPSESSID";s:32:"a6afe8e13507ff74c23c8a41b182c8f4";s:6:"__utma";s:55:"267039964.1054841183.1335450194.1335450194.1335450194.1";s:6:"__utmb";s:25:"267039964.1.10.1335450194";s:6:"__utmc";s:9:"267039964";s:6:"__utmz";s:70:"267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', 'a:1:{s:7:"usuario";a:13:{s:2:"id";s:1:"1";s:7:"usuario";s:5:"heron";s:5:"senha";s:32:"d12e69c36157d034edac61831fb236fc";s:4:"nome";s:12:"Heron Santos";s:5:"email";s:22:"heronrsantos@gmail.com";s:6:"avatar";s:169:"http://www.gravatar.com/avatar/a31833a51eb26f2df5a4d5888e2946ae?size=60&amp;default=http%3A%2F%2Fwww.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fsize%3D60";s:6:"status";s:1:"1";s:12:"datacadastro";s:19:"2012-03-07 11:41:44";s:14:"datamodificado";s:19:"2012-03-09 07:49:18";s:20:"adm_content_nivel_id";s:1:"1";s:15:"datacadastro_br";s:20:"07/03/2012 às 11:41";s:17:"datamodificado_br";s:20:"09/03/2012 às 07:49";s:24:"adm_content_nivel_titulo";s:14:"Super-Usuário";}}', 'a:5:{s:9:"PHPSESSID";s:32:"a6afe8e13507ff74c23c8a41b182c8f4";s:6:"__utma";s:55:"267039964.1054841183.1335450194.1335450194.1335450194.1";s:6:"__utmb";s:25:"267039964.1.10.1335450194";s:6:"__utmc";s:9:"267039964";s:6:"__utmz";s:70:"267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', '2012-04-26 14:32:50', 1, NULL),
(12, 'Carregou página.', 'a:35:{s:13:"DOCUMENT_ROOT";s:24:"/home2/heron/public_html";s:17:"GATEWAY_INTERFACE";s:7:"CGI/1.1";s:11:"HTTP_ACCEPT";s:63:"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";s:20:"HTTP_ACCEPT_ENCODING";s:13:"gzip, deflate";s:20:"HTTP_ACCEPT_LANGUAGE";s:35:"pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3";s:15:"HTTP_CONNECTION";s:10:"keep-alive";s:11:"HTTP_COOKIE";s:237:"PHPSESSID=a6afe8e13507ff74c23c8a41b182c8f4; __utma=267039964.1054841183.1335450194.1335450194.1335450194.1; __utmb=267039964.1.10.1335450194; __utmc=267039964; __utmz=267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";s:9:"HTTP_HOST";s:21:"www.chronos-ti.com.br";s:12:"HTTP_REFERER";s:45:"http://www.chronos-ti.com.br/admin/index.html";s:15:"HTTP_USER_AGENT";s:65:"Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0";s:4:"PATH";s:13:"/bin:/usr/bin";s:16:"PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:12:"QUERY_STRING";s:0:"";s:25:"REDIRECT_PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:15:"REDIRECT_STATUS";s:3:"200";s:18:"REDIRECT_UNIQUE_ID";s:24:"T5lck7tswBAAAH43aKIAAABp";s:12:"REDIRECT_URL";s:18:"/admin/painel.html";s:11:"REMOTE_ADDR";s:12:"157.86.65.93";s:11:"REMOTE_PORT";s:5:"52910";s:14:"REQUEST_METHOD";s:3:"GET";s:11:"REQUEST_URI";s:18:"/admin/painel.html";s:15:"SCRIPT_FILENAME";s:34:"/home2/heron/public_html/index.php";s:11:"SCRIPT_NAME";s:10:"/index.php";s:11:"SERVER_ADDR";s:14:"187.108.192.16";s:12:"SERVER_ADMIN";s:23:"webmaster@kengoo.com.br";s:11:"SERVER_NAME";s:21:"www.chronos-ti.com.br";s:11:"SERVER_PORT";s:2:"80";s:15:"SERVER_PROTOCOL";s:8:"HTTP/1.1";s:16:"SERVER_SIGNATURE";s:0:"";s:15:"SERVER_SOFTWARE";s:6:"Apache";s:9:"UNIQUE_ID";s:24:"T5lck7tswBAAAH43aKIAAABp";s:8:"PHP_SELF";s:10:"/index.php";s:12:"REQUEST_TIME";i:1335450771;s:4:"argv";a:0:{}s:4:"argc";i:0;}', 'a:5:{s:9:"PHPSESSID";s:32:"a6afe8e13507ff74c23c8a41b182c8f4";s:6:"__utma";s:55:"267039964.1054841183.1335450194.1335450194.1335450194.1";s:6:"__utmb";s:25:"267039964.1.10.1335450194";s:6:"__utmc";s:9:"267039964";s:6:"__utmz";s:70:"267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', 'a:1:{s:7:"usuario";a:13:{s:2:"id";s:1:"1";s:7:"usuario";s:5:"heron";s:5:"senha";s:32:"d12e69c36157d034edac61831fb236fc";s:4:"nome";s:12:"Heron Santos";s:5:"email";s:22:"heronrsantos@gmail.com";s:6:"avatar";s:169:"http://www.gravatar.com/avatar/a31833a51eb26f2df5a4d5888e2946ae?size=60&amp;default=http%3A%2F%2Fwww.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fsize%3D60";s:6:"status";s:1:"1";s:12:"datacadastro";s:19:"2012-03-07 11:41:44";s:14:"datamodificado";s:19:"2012-03-09 07:49:18";s:20:"adm_content_nivel_id";s:1:"1";s:15:"datacadastro_br";s:20:"07/03/2012 às 11:41";s:17:"datamodificado_br";s:20:"09/03/2012 às 07:49";s:24:"adm_content_nivel_titulo";s:14:"Super-Usuário";}}', 'a:5:{s:9:"PHPSESSID";s:32:"a6afe8e13507ff74c23c8a41b182c8f4";s:6:"__utma";s:55:"267039964.1054841183.1335450194.1335450194.1335450194.1";s:6:"__utmb";s:25:"267039964.1.10.1335450194";s:6:"__utmc";s:9:"267039964";s:6:"__utmz";s:70:"267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', '2012-04-26 14:32:52', 1, NULL),
(13, 'Carregou página.', 'a:36:{s:13:"DOCUMENT_ROOT";s:24:"/home2/heron/public_html";s:17:"GATEWAY_INTERFACE";s:7:"CGI/1.1";s:11:"HTTP_ACCEPT";s:63:"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";s:20:"HTTP_ACCEPT_ENCODING";s:13:"gzip, deflate";s:20:"HTTP_ACCEPT_LANGUAGE";s:35:"pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3";s:15:"HTTP_CONNECTION";s:10:"keep-alive";s:11:"HTTP_COOKIE";s:237:"PHPSESSID=a6afe8e13507ff74c23c8a41b182c8f4; __utma=267039964.1054841183.1335450194.1335450194.1335450194.1; __utmb=267039964.1.10.1335450194; __utmc=267039964; __utmz=267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";s:9:"HTTP_HOST";s:21:"www.chronos-ti.com.br";s:12:"HTTP_REFERER";s:46:"http://www.chronos-ti.com.br/admin/painel.html";s:15:"HTTP_USER_AGENT";s:65:"Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0";s:4:"PATH";s:13:"/bin:/usr/bin";s:16:"PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:12:"QUERY_STRING";s:20:"content=conta/editar";s:25:"REDIRECT_PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:21:"REDIRECT_QUERY_STRING";s:20:"content=conta/editar";s:15:"REDIRECT_STATUS";s:3:"200";s:18:"REDIRECT_UNIQUE_ID";s:24:"T5ld6btswBAAAAsSJz0AAABu";s:12:"REDIRECT_URL";s:18:"/admin/painel.html";s:11:"REMOTE_ADDR";s:12:"157.86.65.93";s:11:"REMOTE_PORT";s:5:"52964";s:14:"REQUEST_METHOD";s:3:"GET";s:11:"REQUEST_URI";s:39:"/admin/painel.html?content=conta/editar";s:15:"SCRIPT_FILENAME";s:34:"/home2/heron/public_html/index.php";s:11:"SCRIPT_NAME";s:10:"/index.php";s:11:"SERVER_ADDR";s:14:"187.108.192.16";s:12:"SERVER_ADMIN";s:23:"webmaster@kengoo.com.br";s:11:"SERVER_NAME";s:21:"www.chronos-ti.com.br";s:11:"SERVER_PORT";s:2:"80";s:15:"SERVER_PROTOCOL";s:8:"HTTP/1.1";s:16:"SERVER_SIGNATURE";s:0:"";s:15:"SERVER_SOFTWARE";s:6:"Apache";s:9:"UNIQUE_ID";s:24:"T5ld6btswBAAAAsSJz0AAABu";s:8:"PHP_SELF";s:10:"/index.php";s:12:"REQUEST_TIME";i:1335451113;s:4:"argv";a:1:{i:0;s:20:"content=conta/editar";}s:4:"argc";i:1;}', 'a:6:{s:7:"content";s:12:"conta/editar";s:9:"PHPSESSID";s:32:"a6afe8e13507ff74c23c8a41b182c8f4";s:6:"__utma";s:55:"267039964.1054841183.1335450194.1335450194.1335450194.1";s:6:"__utmb";s:25:"267039964.1.10.1335450194";s:6:"__utmc";s:9:"267039964";s:6:"__utmz";s:70:"267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', 'a:2:{s:7:"usuario";a:13:{s:2:"id";s:1:"1";s:7:"usuario";s:5:"heron";s:5:"senha";s:32:"d12e69c36157d034edac61831fb236fc";s:4:"nome";s:12:"Heron Santos";s:5:"email";s:22:"heronrsantos@gmail.com";s:6:"avatar";s:169:"http://www.gravatar.com/avatar/a31833a51eb26f2df5a4d5888e2946ae?size=60&amp;default=http%3A%2F%2Fwww.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fsize%3D60";s:6:"status";s:1:"1";s:12:"datacadastro";s:19:"2012-03-07 11:41:44";s:14:"datamodificado";s:19:"2012-03-09 07:49:18";s:20:"adm_content_nivel_id";s:1:"1";s:15:"datacadastro_br";s:20:"07/03/2012 às 11:41";s:17:"datamodificado_br";s:20:"09/03/2012 às 07:49";s:24:"adm_content_nivel_titulo";s:14:"Super-Usuário";}s:11:"adm_content";a:12:{s:2:"id";s:2:"37";s:7:"content";s:12:"conta/editar";s:6:"titulo";s:11:"Minha Conta";s:9:"ordenacao";s:1:"0";s:6:"status";s:1:"1";s:12:"datacadastro";s:19:"2012-03-09 06:07:30";s:14:"datamodificado";s:19:"2012-03-09 06:07:30";s:14:"adm_content_id";N;s:20:"adm_content_nivel_id";s:1:"5";s:15:"datacadastro_br";s:20:"09/03/2012 às 06:07";s:17:"datamodificado_br";s:20:"09/03/2012 às 06:07";s:24:"adm_content_nivel_titulo";s:8:"Operador";}}', 'a:5:{s:9:"PHPSESSID";s:32:"a6afe8e13507ff74c23c8a41b182c8f4";s:6:"__utma";s:55:"267039964.1054841183.1335450194.1335450194.1335450194.1";s:6:"__utmb";s:25:"267039964.1.10.1335450194";s:6:"__utmc";s:9:"267039964";s:6:"__utmz";s:70:"267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', '2012-04-26 14:38:34', 1, 37),
(14, 'Carregou página.', 'a:35:{s:13:"DOCUMENT_ROOT";s:24:"/home2/heron/public_html";s:17:"GATEWAY_INTERFACE";s:7:"CGI/1.1";s:11:"HTTP_ACCEPT";s:63:"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";s:20:"HTTP_ACCEPT_ENCODING";s:13:"gzip, deflate";s:20:"HTTP_ACCEPT_LANGUAGE";s:35:"pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3";s:15:"HTTP_CONNECTION";s:10:"keep-alive";s:11:"HTTP_COOKIE";s:237:"PHPSESSID=a6afe8e13507ff74c23c8a41b182c8f4; __utma=267039964.1054841183.1335450194.1335450194.1335450194.1; __utmb=267039964.1.10.1335450194; __utmc=267039964; __utmz=267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";s:9:"HTTP_HOST";s:21:"www.chronos-ti.com.br";s:12:"HTTP_REFERER";s:67:"http://www.chronos-ti.com.br/admin/painel.html?content=conta/editar";s:15:"HTTP_USER_AGENT";s:65:"Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0";s:4:"PATH";s:13:"/bin:/usr/bin";s:16:"PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:12:"QUERY_STRING";s:0:"";s:25:"REDIRECT_PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:15:"REDIRECT_STATUS";s:3:"200";s:18:"REDIRECT_UNIQUE_ID";s:24:"T5leErtswBAAABOXiCEAAABX";s:12:"REDIRECT_URL";s:18:"/admin/painel.html";s:11:"REMOTE_ADDR";s:12:"157.86.65.93";s:11:"REMOTE_PORT";s:5:"52988";s:14:"REQUEST_METHOD";s:3:"GET";s:11:"REQUEST_URI";s:18:"/admin/painel.html";s:15:"SCRIPT_FILENAME";s:34:"/home2/heron/public_html/index.php";s:11:"SCRIPT_NAME";s:10:"/index.php";s:11:"SERVER_ADDR";s:14:"187.108.192.16";s:12:"SERVER_ADMIN";s:23:"webmaster@kengoo.com.br";s:11:"SERVER_NAME";s:21:"www.chronos-ti.com.br";s:11:"SERVER_PORT";s:2:"80";s:15:"SERVER_PROTOCOL";s:8:"HTTP/1.1";s:16:"SERVER_SIGNATURE";s:0:"";s:15:"SERVER_SOFTWARE";s:6:"Apache";s:9:"UNIQUE_ID";s:24:"T5leErtswBAAABOXiCEAAABX";s:8:"PHP_SELF";s:10:"/index.php";s:12:"REQUEST_TIME";i:1335451154;s:4:"argv";a:0:{}s:4:"argc";i:0;}', 'a:5:{s:9:"PHPSESSID";s:32:"a6afe8e13507ff74c23c8a41b182c8f4";s:6:"__utma";s:55:"267039964.1054841183.1335450194.1335450194.1335450194.1";s:6:"__utmb";s:25:"267039964.1.10.1335450194";s:6:"__utmc";s:9:"267039964";s:6:"__utmz";s:70:"267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', 'a:2:{s:7:"usuario";a:13:{s:2:"id";s:1:"1";s:7:"usuario";s:5:"heron";s:5:"senha";s:32:"d12e69c36157d034edac61831fb236fc";s:4:"nome";s:12:"Heron Santos";s:5:"email";s:22:"heronrsantos@gmail.com";s:6:"avatar";s:169:"http://www.gravatar.com/avatar/a31833a51eb26f2df5a4d5888e2946ae?size=60&amp;default=http%3A%2F%2Fwww.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fsize%3D60";s:6:"status";s:1:"1";s:12:"datacadastro";s:19:"2012-03-07 11:41:44";s:14:"datamodificado";s:19:"2012-03-09 07:49:18";s:20:"adm_content_nivel_id";s:1:"1";s:15:"datacadastro_br";s:20:"07/03/2012 às 11:41";s:17:"datamodificado_br";s:20:"09/03/2012 às 07:49";s:24:"adm_content_nivel_titulo";s:14:"Super-Usuário";}s:11:"adm_content";a:12:{s:2:"id";s:2:"37";s:7:"content";s:12:"conta/editar";s:6:"titulo";s:11:"Minha Conta";s:9:"ordenacao";s:1:"0";s:6:"status";s:1:"1";s:12:"datacadastro";s:19:"2012-03-09 06:07:30";s:14:"datamodificado";s:19:"2012-03-09 06:07:30";s:14:"adm_content_id";N;s:20:"adm_content_nivel_id";s:1:"5";s:15:"datacadastro_br";s:20:"09/03/2012 às 06:07";s:17:"datamodificado_br";s:20:"09/03/2012 às 06:07";s:24:"adm_content_nivel_titulo";s:8:"Operador";}}', 'a:5:{s:9:"PHPSESSID";s:32:"a6afe8e13507ff74c23c8a41b182c8f4";s:6:"__utma";s:55:"267039964.1054841183.1335450194.1335450194.1335450194.1";s:6:"__utmb";s:25:"267039964.1.10.1335450194";s:6:"__utmc";s:9:"267039964";s:6:"__utmz";s:70:"267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', '2012-04-26 14:39:14', 1, 37),
(15, 'Carregou página.', 'a:36:{s:13:"DOCUMENT_ROOT";s:24:"/home2/heron/public_html";s:17:"GATEWAY_INTERFACE";s:7:"CGI/1.1";s:11:"HTTP_ACCEPT";s:63:"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";s:20:"HTTP_ACCEPT_ENCODING";s:13:"gzip, deflate";s:20:"HTTP_ACCEPT_LANGUAGE";s:35:"pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3";s:15:"HTTP_CONNECTION";s:10:"keep-alive";s:11:"HTTP_COOKIE";s:237:"PHPSESSID=a6afe8e13507ff74c23c8a41b182c8f4; __utma=267039964.1054841183.1335450194.1335450194.1335450194.1; __utmb=267039964.1.10.1335450194; __utmc=267039964; __utmz=267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";s:9:"HTTP_HOST";s:21:"www.chronos-ti.com.br";s:12:"HTTP_REFERER";s:46:"http://www.chronos-ti.com.br/admin/painel.html";s:15:"HTTP_USER_AGENT";s:65:"Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0";s:4:"PATH";s:13:"/bin:/usr/bin";s:16:"PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:12:"QUERY_STRING";s:16:"content=chamados";s:25:"REDIRECT_PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:21:"REDIRECT_QUERY_STRING";s:16:"content=chamados";s:15:"REDIRECT_STATUS";s:3:"200";s:18:"REDIRECT_UNIQUE_ID";s:24:"T5leK7tswBAAABbIL68AAAD6";s:12:"REDIRECT_URL";s:18:"/admin/painel.html";s:11:"REMOTE_ADDR";s:12:"157.86.65.93";s:11:"REMOTE_PORT";s:5:"52994";s:14:"REQUEST_METHOD";s:3:"GET";s:11:"REQUEST_URI";s:35:"/admin/painel.html?content=chamados";s:15:"SCRIPT_FILENAME";s:34:"/home2/heron/public_html/index.php";s:11:"SCRIPT_NAME";s:10:"/index.php";s:11:"SERVER_ADDR";s:14:"187.108.192.16";s:12:"SERVER_ADMIN";s:23:"webmaster@kengoo.com.br";s:11:"SERVER_NAME";s:21:"www.chronos-ti.com.br";s:11:"SERVER_PORT";s:2:"80";s:15:"SERVER_PROTOCOL";s:8:"HTTP/1.1";s:16:"SERVER_SIGNATURE";s:0:"";s:15:"SERVER_SOFTWARE";s:6:"Apache";s:9:"UNIQUE_ID";s:24:"T5leK7tswBAAABbIL68AAAD6";s:8:"PHP_SELF";s:10:"/index.php";s:12:"REQUEST_TIME";i:1335451179;s:4:"argv";a:1:{i:0;s:16:"content=chamados";}s:4:"argc";i:1;}', 'a:6:{s:7:"content";s:8:"chamados";s:9:"PHPSESSID";s:32:"a6afe8e13507ff74c23c8a41b182c8f4";s:6:"__utma";s:55:"267039964.1054841183.1335450194.1335450194.1335450194.1";s:6:"__utmb";s:25:"267039964.1.10.1335450194";s:6:"__utmc";s:9:"267039964";s:6:"__utmz";s:70:"267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', 'a:2:{s:7:"usuario";a:13:{s:2:"id";s:1:"1";s:7:"usuario";s:5:"heron";s:5:"senha";s:32:"d12e69c36157d034edac61831fb236fc";s:4:"nome";s:12:"Heron Santos";s:5:"email";s:22:"heronrsantos@gmail.com";s:6:"avatar";s:169:"http://www.gravatar.com/avatar/a31833a51eb26f2df5a4d5888e2946ae?size=60&amp;default=http%3A%2F%2Fwww.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fsize%3D60";s:6:"status";s:1:"1";s:12:"datacadastro";s:19:"2012-03-07 11:41:44";s:14:"datamodificado";s:19:"2012-03-09 07:49:18";s:20:"adm_content_nivel_id";s:1:"1";s:15:"datacadastro_br";s:20:"07/03/2012 às 11:41";s:17:"datamodificado_br";s:20:"09/03/2012 às 07:49";s:24:"adm_content_nivel_titulo";s:14:"Super-Usuário";}s:11:"adm_content";a:12:{s:2:"id";s:1:"2";s:7:"content";s:8:"chamados";s:6:"titulo";s:8:"Chamados";s:9:"ordenacao";s:1:"2";s:6:"status";s:1:"1";s:12:"datacadastro";s:19:"2012-03-07 11:37:15";s:14:"datamodificado";s:19:"2012-03-07 11:40:37";s:14:"adm_content_id";N;s:20:"adm_content_nivel_id";s:1:"5";s:15:"datacadastro_br";s:20:"07/03/2012 às 11:37";s:17:"datamodificado_br";s:20:"07/03/2012 às 11:40";s:24:"adm_content_nivel_titulo";s:8:"Operador";}}', 'a:5:{s:9:"PHPSESSID";s:32:"a6afe8e13507ff74c23c8a41b182c8f4";s:6:"__utma";s:55:"267039964.1054841183.1335450194.1335450194.1335450194.1";s:6:"__utmb";s:25:"267039964.1.10.1335450194";s:6:"__utmc";s:9:"267039964";s:6:"__utmz";s:70:"267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', '2012-04-26 14:39:40', 1, 2),
(16, 'Carregou página.', 'a:36:{s:13:"DOCUMENT_ROOT";s:24:"/home2/heron/public_html";s:17:"GATEWAY_INTERFACE";s:7:"CGI/1.1";s:11:"HTTP_ACCEPT";s:63:"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";s:20:"HTTP_ACCEPT_ENCODING";s:13:"gzip, deflate";s:20:"HTTP_ACCEPT_LANGUAGE";s:35:"pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3";s:15:"HTTP_CONNECTION";s:10:"keep-alive";s:11:"HTTP_COOKIE";s:237:"PHPSESSID=a6afe8e13507ff74c23c8a41b182c8f4; __utma=267039964.1054841183.1335450194.1335450194.1335450194.1; __utmb=267039964.1.10.1335450194; __utmc=267039964; __utmz=267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";s:9:"HTTP_HOST";s:21:"www.chronos-ti.com.br";s:12:"HTTP_REFERER";s:46:"http://www.chronos-ti.com.br/admin/painel.html";s:15:"HTTP_USER_AGENT";s:65:"Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0";s:4:"PATH";s:13:"/bin:/usr/bin";s:16:"PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:12:"QUERY_STRING";s:16:"content=chamados";s:25:"REDIRECT_PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:21:"REDIRECT_QUERY_STRING";s:16:"content=chamados";s:15:"REDIRECT_STATUS";s:3:"200";s:18:"REDIRECT_UNIQUE_ID";s:24:"T5leN7tswBAAABPMzJUAAADa";s:12:"REDIRECT_URL";s:18:"/admin/painel.html";s:11:"REMOTE_ADDR";s:12:"157.86.65.93";s:11:"REMOTE_PORT";s:5:"53000";s:14:"REQUEST_METHOD";s:3:"GET";s:11:"REQUEST_URI";s:35:"/admin/painel.html?content=chamados";s:15:"SCRIPT_FILENAME";s:34:"/home2/heron/public_html/index.php";s:11:"SCRIPT_NAME";s:10:"/index.php";s:11:"SERVER_ADDR";s:14:"187.108.192.16";s:12:"SERVER_ADMIN";s:23:"webmaster@kengoo.com.br";s:11:"SERVER_NAME";s:21:"www.chronos-ti.com.br";s:11:"SERVER_PORT";s:2:"80";s:15:"SERVER_PROTOCOL";s:8:"HTTP/1.1";s:16:"SERVER_SIGNATURE";s:0:"";s:15:"SERVER_SOFTWARE";s:6:"Apache";s:9:"UNIQUE_ID";s:24:"T5leN7tswBAAABPMzJUAAADa";s:8:"PHP_SELF";s:10:"/index.php";s:12:"REQUEST_TIME";i:1335451191;s:4:"argv";a:1:{i:0;s:16:"content=chamados";}s:4:"argc";i:1;}', 'a:6:{s:7:"content";s:8:"chamados";s:9:"PHPSESSID";s:32:"a6afe8e13507ff74c23c8a41b182c8f4";s:6:"__utma";s:55:"267039964.1054841183.1335450194.1335450194.1335450194.1";s:6:"__utmb";s:25:"267039964.1.10.1335450194";s:6:"__utmc";s:9:"267039964";s:6:"__utmz";s:70:"267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', 'a:2:{s:7:"usuario";a:13:{s:2:"id";s:1:"1";s:7:"usuario";s:5:"heron";s:5:"senha";s:32:"d12e69c36157d034edac61831fb236fc";s:4:"nome";s:12:"Heron Santos";s:5:"email";s:22:"heronrsantos@gmail.com";s:6:"avatar";s:169:"http://www.gravatar.com/avatar/a31833a51eb26f2df5a4d5888e2946ae?size=60&amp;default=http%3A%2F%2Fwww.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fsize%3D60";s:6:"status";s:1:"1";s:12:"datacadastro";s:19:"2012-03-07 11:41:44";s:14:"datamodificado";s:19:"2012-03-09 07:49:18";s:20:"adm_content_nivel_id";s:1:"1";s:15:"datacadastro_br";s:20:"07/03/2012 às 11:41";s:17:"datamodificado_br";s:20:"09/03/2012 às 07:49";s:24:"adm_content_nivel_titulo";s:14:"Super-Usuário";}s:11:"adm_content";a:12:{s:2:"id";s:1:"2";s:7:"content";s:8:"chamados";s:6:"titulo";s:8:"Chamados";s:9:"ordenacao";s:1:"2";s:6:"status";s:1:"1";s:12:"datacadastro";s:19:"2012-03-07 11:37:15";s:14:"datamodificado";s:19:"2012-03-07 11:40:37";s:14:"adm_content_id";N;s:20:"adm_content_nivel_id";s:1:"5";s:15:"datacadastro_br";s:20:"07/03/2012 às 11:37";s:17:"datamodificado_br";s:20:"07/03/2012 às 11:40";s:24:"adm_content_nivel_titulo";s:8:"Operador";}}', 'a:5:{s:9:"PHPSESSID";s:32:"a6afe8e13507ff74c23c8a41b182c8f4";s:6:"__utma";s:55:"267039964.1054841183.1335450194.1335450194.1335450194.1";s:6:"__utmb";s:25:"267039964.1.10.1335450194";s:6:"__utmc";s:9:"267039964";s:6:"__utmz";s:70:"267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', '2012-04-26 14:39:51', 1, 2);
INSERT INTO `historico` (`id`, `acao`, `server_variables`, `request_variables`, `session_variables`, `cookie_variables`, `data`, `usuarios_id`, `adm_content_id`) VALUES
(17, 'Carregou página.', 'a:36:{s:13:"DOCUMENT_ROOT";s:24:"/home2/heron/public_html";s:17:"GATEWAY_INTERFACE";s:7:"CGI/1.1";s:11:"HTTP_ACCEPT";s:63:"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";s:20:"HTTP_ACCEPT_ENCODING";s:13:"gzip, deflate";s:20:"HTTP_ACCEPT_LANGUAGE";s:35:"pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3";s:15:"HTTP_CONNECTION";s:10:"keep-alive";s:11:"HTTP_COOKIE";s:237:"PHPSESSID=a6afe8e13507ff74c23c8a41b182c8f4; __utma=267039964.1054841183.1335450194.1335450194.1335450194.1; __utmb=267039964.1.10.1335450194; __utmc=267039964; __utmz=267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";s:9:"HTTP_HOST";s:21:"www.chronos-ti.com.br";s:12:"HTTP_REFERER";s:46:"http://www.chronos-ti.com.br/admin/painel.html";s:15:"HTTP_USER_AGENT";s:65:"Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0";s:4:"PATH";s:13:"/bin:/usr/bin";s:16:"PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:12:"QUERY_STRING";s:16:"content=chamados";s:25:"REDIRECT_PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:21:"REDIRECT_QUERY_STRING";s:16:"content=chamados";s:15:"REDIRECT_STATUS";s:3:"200";s:18:"REDIRECT_UNIQUE_ID";s:24:"T5lfqbtswBAAAD0-KgoAAADr";s:12:"REDIRECT_URL";s:18:"/admin/painel.html";s:11:"REMOTE_ADDR";s:12:"157.86.65.93";s:11:"REMOTE_PORT";s:5:"53051";s:14:"REQUEST_METHOD";s:3:"GET";s:11:"REQUEST_URI";s:35:"/admin/painel.html?content=chamados";s:15:"SCRIPT_FILENAME";s:34:"/home2/heron/public_html/index.php";s:11:"SCRIPT_NAME";s:10:"/index.php";s:11:"SERVER_ADDR";s:14:"187.108.192.16";s:12:"SERVER_ADMIN";s:23:"webmaster@kengoo.com.br";s:11:"SERVER_NAME";s:21:"www.chronos-ti.com.br";s:11:"SERVER_PORT";s:2:"80";s:15:"SERVER_PROTOCOL";s:8:"HTTP/1.1";s:16:"SERVER_SIGNATURE";s:0:"";s:15:"SERVER_SOFTWARE";s:6:"Apache";s:9:"UNIQUE_ID";s:24:"T5lfqbtswBAAAD0-KgoAAADr";s:8:"PHP_SELF";s:10:"/index.php";s:12:"REQUEST_TIME";i:1335451561;s:4:"argv";a:1:{i:0;s:16:"content=chamados";}s:4:"argc";i:1;}', 'a:6:{s:7:"content";s:8:"chamados";s:9:"PHPSESSID";s:32:"a6afe8e13507ff74c23c8a41b182c8f4";s:6:"__utma";s:55:"267039964.1054841183.1335450194.1335450194.1335450194.1";s:6:"__utmb";s:25:"267039964.1.10.1335450194";s:6:"__utmc";s:9:"267039964";s:6:"__utmz";s:70:"267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', 'a:2:{s:7:"usuario";a:13:{s:2:"id";s:1:"1";s:7:"usuario";s:5:"heron";s:5:"senha";s:32:"d12e69c36157d034edac61831fb236fc";s:4:"nome";s:12:"Heron Santos";s:5:"email";s:22:"heronrsantos@gmail.com";s:6:"avatar";s:169:"http://www.gravatar.com/avatar/a31833a51eb26f2df5a4d5888e2946ae?size=60&amp;default=http%3A%2F%2Fwww.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fsize%3D60";s:6:"status";s:1:"1";s:12:"datacadastro";s:19:"2012-03-07 11:41:44";s:14:"datamodificado";s:19:"2012-03-09 07:49:18";s:20:"adm_content_nivel_id";s:1:"1";s:15:"datacadastro_br";s:20:"07/03/2012 às 11:41";s:17:"datamodificado_br";s:20:"09/03/2012 às 07:49";s:24:"adm_content_nivel_titulo";s:14:"Super-Usuário";}s:11:"adm_content";a:12:{s:2:"id";s:1:"2";s:7:"content";s:8:"chamados";s:6:"titulo";s:8:"Chamados";s:9:"ordenacao";s:1:"2";s:6:"status";s:1:"1";s:12:"datacadastro";s:19:"2012-03-07 11:37:15";s:14:"datamodificado";s:19:"2012-03-07 11:40:37";s:14:"adm_content_id";N;s:20:"adm_content_nivel_id";s:1:"5";s:15:"datacadastro_br";s:20:"07/03/2012 às 11:37";s:17:"datamodificado_br";s:20:"07/03/2012 às 11:40";s:24:"adm_content_nivel_titulo";s:8:"Operador";}}', 'a:5:{s:9:"PHPSESSID";s:32:"a6afe8e13507ff74c23c8a41b182c8f4";s:6:"__utma";s:55:"267039964.1054841183.1335450194.1335450194.1335450194.1";s:6:"__utmb";s:25:"267039964.1.10.1335450194";s:6:"__utmc";s:9:"267039964";s:6:"__utmz";s:70:"267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', '2012-04-26 14:46:01', 1, 2),
(18, 'Carregou página.', 'a:34:{s:13:"DOCUMENT_ROOT";s:24:"/home2/heron/public_html";s:17:"GATEWAY_INTERFACE";s:7:"CGI/1.1";s:11:"HTTP_ACCEPT";s:63:"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";s:20:"HTTP_ACCEPT_ENCODING";s:13:"gzip, deflate";s:20:"HTTP_ACCEPT_LANGUAGE";s:35:"pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3";s:15:"HTTP_CONNECTION";s:10:"keep-alive";s:11:"HTTP_COOKIE";s:237:"PHPSESSID=a6afe8e13507ff74c23c8a41b182c8f4; __utma=267039964.1054841183.1335450194.1335450194.1335450194.1; __utmb=267039964.1.10.1335450194; __utmc=267039964; __utmz=267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";s:9:"HTTP_HOST";s:21:"www.chronos-ti.com.br";s:15:"HTTP_USER_AGENT";s:65:"Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0";s:4:"PATH";s:13:"/bin:/usr/bin";s:16:"PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:12:"QUERY_STRING";s:0:"";s:25:"REDIRECT_PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:15:"REDIRECT_STATUS";s:3:"200";s:18:"REDIRECT_UNIQUE_ID";s:24:"T5lgYbtswBAAABbEKqUAAAD2";s:12:"REDIRECT_URL";s:18:"/admin/painel.html";s:11:"REMOTE_ADDR";s:12:"157.86.65.93";s:11:"REMOTE_PORT";s:5:"53148";s:14:"REQUEST_METHOD";s:3:"GET";s:11:"REQUEST_URI";s:18:"/admin/painel.html";s:15:"SCRIPT_FILENAME";s:34:"/home2/heron/public_html/index.php";s:11:"SCRIPT_NAME";s:10:"/index.php";s:11:"SERVER_ADDR";s:14:"187.108.192.16";s:12:"SERVER_ADMIN";s:23:"webmaster@kengoo.com.br";s:11:"SERVER_NAME";s:21:"www.chronos-ti.com.br";s:11:"SERVER_PORT";s:2:"80";s:15:"SERVER_PROTOCOL";s:8:"HTTP/1.1";s:16:"SERVER_SIGNATURE";s:0:"";s:15:"SERVER_SOFTWARE";s:6:"Apache";s:9:"UNIQUE_ID";s:24:"T5lgYbtswBAAABbEKqUAAAD2";s:8:"PHP_SELF";s:10:"/index.php";s:12:"REQUEST_TIME";i:1335451745;s:4:"argv";a:0:{}s:4:"argc";i:0;}', 'a:5:{s:9:"PHPSESSID";s:32:"a6afe8e13507ff74c23c8a41b182c8f4";s:6:"__utma";s:55:"267039964.1054841183.1335450194.1335450194.1335450194.1";s:6:"__utmb";s:25:"267039964.1.10.1335450194";s:6:"__utmc";s:9:"267039964";s:6:"__utmz";s:70:"267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', 'a:2:{s:7:"usuario";a:13:{s:2:"id";s:1:"1";s:7:"usuario";s:5:"heron";s:5:"senha";s:32:"d12e69c36157d034edac61831fb236fc";s:4:"nome";s:12:"Heron Santos";s:5:"email";s:22:"heronrsantos@gmail.com";s:6:"avatar";s:169:"http://www.gravatar.com/avatar/a31833a51eb26f2df5a4d5888e2946ae?size=60&amp;default=http%3A%2F%2Fwww.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fsize%3D60";s:6:"status";s:1:"1";s:12:"datacadastro";s:19:"2012-03-07 11:41:44";s:14:"datamodificado";s:19:"2012-03-09 07:49:18";s:20:"adm_content_nivel_id";s:1:"1";s:15:"datacadastro_br";s:20:"07/03/2012 às 11:41";s:17:"datamodificado_br";s:20:"09/03/2012 às 07:49";s:24:"adm_content_nivel_titulo";s:14:"Super-Usuário";}s:11:"adm_content";a:12:{s:2:"id";s:1:"2";s:7:"content";s:8:"chamados";s:6:"titulo";s:8:"Chamados";s:9:"ordenacao";s:1:"2";s:6:"status";s:1:"1";s:12:"datacadastro";s:19:"2012-03-07 11:37:15";s:14:"datamodificado";s:19:"2012-03-07 11:40:37";s:14:"adm_content_id";N;s:20:"adm_content_nivel_id";s:1:"5";s:15:"datacadastro_br";s:20:"07/03/2012 às 11:37";s:17:"datamodificado_br";s:20:"07/03/2012 às 11:40";s:24:"adm_content_nivel_titulo";s:8:"Operador";}}', 'a:5:{s:9:"PHPSESSID";s:32:"a6afe8e13507ff74c23c8a41b182c8f4";s:6:"__utma";s:55:"267039964.1054841183.1335450194.1335450194.1335450194.1";s:6:"__utmb";s:25:"267039964.1.10.1335450194";s:6:"__utmc";s:9:"267039964";s:6:"__utmz";s:70:"267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";}', '2012-04-26 14:49:05', 1, 2),
(19, 'Problemas ao efetuar login.', 'a:37:{s:14:"CONTENT_LENGTH";s:2:"39";s:12:"CONTENT_TYPE";s:33:"application/x-www-form-urlencoded";s:13:"DOCUMENT_ROOT";s:24:"/home2/heron/public_html";s:17:"GATEWAY_INTERFACE";s:7:"CGI/1.1";s:11:"HTTP_ACCEPT";s:63:"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";s:20:"HTTP_ACCEPT_ENCODING";s:13:"gzip, deflate";s:20:"HTTP_ACCEPT_LANGUAGE";s:14:"en-us,en;q=0.5";s:15:"HTTP_CONNECTION";s:10:"keep-alive";s:11:"HTTP_COOKIE";s:42:"PHPSESSID=af7f824b29fa99fc39f40168cfdc8e37";s:9:"HTTP_HOST";s:21:"www.chronos-ti.com.br";s:12:"HTTP_REFERER";s:45:"http://www.chronos-ti.com.br/admin/index.html";s:15:"HTTP_USER_AGENT";s:76:"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:12.0) Gecko/20100101 Firefox/12.0";s:4:"PATH";s:13:"/bin:/usr/bin";s:16:"PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:12:"QUERY_STRING";s:0:"";s:25:"REDIRECT_PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:15:"REDIRECT_STATUS";s:3:"200";s:18:"REDIRECT_UNIQUE_ID";s:24:"T5rsw7tswBAAAD0EIlwAAABb";s:12:"REDIRECT_URL";s:17:"/admin/index.html";s:11:"REMOTE_ADDR";s:13:"157.86.65.228";s:11:"REMOTE_PORT";s:5:"60703";s:14:"REQUEST_METHOD";s:4:"POST";s:11:"REQUEST_URI";s:17:"/admin/index.html";s:15:"SCRIPT_FILENAME";s:34:"/home2/heron/public_html/index.php";s:11:"SCRIPT_NAME";s:10:"/index.php";s:11:"SERVER_ADDR";s:14:"187.108.192.16";s:12:"SERVER_ADMIN";s:23:"webmaster@kengoo.com.br";s:11:"SERVER_NAME";s:21:"www.chronos-ti.com.br";s:11:"SERVER_PORT";s:2:"80";s:15:"SERVER_PROTOCOL";s:8:"HTTP/1.1";s:16:"SERVER_SIGNATURE";s:0:"";s:15:"SERVER_SOFTWARE";s:6:"Apache";s:9:"UNIQUE_ID";s:24:"T5rsw7tswBAAAD0EIlwAAABb";s:8:"PHP_SELF";s:10:"/index.php";s:12:"REQUEST_TIME";i:1335553219;s:4:"argv";a:0:{}s:4:"argc";i:0;}', 'a:3:{s:8:"username";s:12:"heronrsantos";s:8:"password";s:8:"skar2006";s:9:"PHPSESSID";s:32:"af7f824b29fa99fc39f40168cfdc8e37";}', 'a:0:{}', 'a:1:{s:9:"PHPSESSID";s:32:"af7f824b29fa99fc39f40168cfdc8e37";}', '2012-04-27 19:00:21', NULL, NULL),
(20, 'Problemas ao efetuar login.', 'a:37:{s:14:"CONTENT_LENGTH";s:2:"39";s:12:"CONTENT_TYPE";s:33:"application/x-www-form-urlencoded";s:13:"DOCUMENT_ROOT";s:24:"/home2/heron/public_html";s:17:"GATEWAY_INTERFACE";s:7:"CGI/1.1";s:11:"HTTP_ACCEPT";s:63:"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";s:20:"HTTP_ACCEPT_ENCODING";s:13:"gzip, deflate";s:20:"HTTP_ACCEPT_LANGUAGE";s:14:"en-us,en;q=0.5";s:15:"HTTP_CONNECTION";s:10:"keep-alive";s:11:"HTTP_COOKIE";s:42:"PHPSESSID=af7f824b29fa99fc39f40168cfdc8e37";s:9:"HTTP_HOST";s:21:"www.chronos-ti.com.br";s:12:"HTTP_REFERER";s:45:"http://www.chronos-ti.com.br/admin/index.html";s:15:"HTTP_USER_AGENT";s:76:"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:12.0) Gecko/20100101 Firefox/12.0";s:4:"PATH";s:13:"/bin:/usr/bin";s:16:"PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:12:"QUERY_STRING";s:0:"";s:25:"REDIRECT_PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:15:"REDIRECT_STATUS";s:3:"200";s:18:"REDIRECT_UNIQUE_ID";s:24:"T5rs5LtswBAAAD4rwjEAAACr";s:12:"REDIRECT_URL";s:17:"/admin/index.html";s:11:"REMOTE_ADDR";s:13:"157.86.65.228";s:11:"REMOTE_PORT";s:5:"62155";s:14:"REQUEST_METHOD";s:4:"POST";s:11:"REQUEST_URI";s:17:"/admin/index.html";s:15:"SCRIPT_FILENAME";s:34:"/home2/heron/public_html/index.php";s:11:"SCRIPT_NAME";s:10:"/index.php";s:11:"SERVER_ADDR";s:14:"187.108.192.16";s:12:"SERVER_ADMIN";s:23:"webmaster@kengoo.com.br";s:11:"SERVER_NAME";s:21:"www.chronos-ti.com.br";s:11:"SERVER_PORT";s:2:"80";s:15:"SERVER_PROTOCOL";s:8:"HTTP/1.1";s:16:"SERVER_SIGNATURE";s:0:"";s:15:"SERVER_SOFTWARE";s:6:"Apache";s:9:"UNIQUE_ID";s:24:"T5rs5LtswBAAAD4rwjEAAACr";s:8:"PHP_SELF";s:10:"/index.php";s:12:"REQUEST_TIME";i:1335553253;s:4:"argv";a:0:{}s:4:"argc";i:0;}', 'a:3:{s:8:"username";s:12:"heronrsantos";s:8:"password";s:8:"b9295993";s:9:"PHPSESSID";s:32:"af7f824b29fa99fc39f40168cfdc8e37";}', 'a:0:{}', 'a:1:{s:9:"PHPSESSID";s:32:"af7f824b29fa99fc39f40168cfdc8e37";}', '2012-04-27 19:00:53', NULL, NULL),
(21, 'Problemas ao efetuar login.', 'a:37:{s:14:"CONTENT_LENGTH";s:2:"39";s:12:"CONTENT_TYPE";s:33:"application/x-www-form-urlencoded";s:13:"DOCUMENT_ROOT";s:24:"/home2/heron/public_html";s:17:"GATEWAY_INTERFACE";s:7:"CGI/1.1";s:11:"HTTP_ACCEPT";s:63:"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";s:20:"HTTP_ACCEPT_ENCODING";s:13:"gzip, deflate";s:20:"HTTP_ACCEPT_LANGUAGE";s:26:"pt-br,en-us;q=0.7,en;q=0.3";s:15:"HTTP_CONNECTION";s:10:"keep-alive";s:11:"HTTP_COOKIE";s:237:"__utma=267039964.1054841183.1335450194.1335450194.1335982421.2; __utmz=267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); PHPSESSID=88febfa30ffcb08bf731bea1ff83bc7d; __utmb=267039964.1.10.1335982421; __utmc=267039964";s:9:"HTTP_HOST";s:21:"www.chronos-ti.com.br";s:12:"HTTP_REFERER";s:45:"http://www.chronos-ti.com.br/admin/index.html";s:15:"HTTP_USER_AGENT";s:65:"Mozilla/5.0 (Windows NT 6.1; rv:12.0) Gecko/20100101 Firefox/12.0";s:4:"PATH";s:13:"/bin:/usr/bin";s:16:"PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:12:"QUERY_STRING";s:0:"";s:25:"REDIRECT_PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:15:"REDIRECT_STATUS";s:3:"200";s:18:"REDIRECT_UNIQUE_ID";s:24:"T6F5X7tswBAAAD8Hd5AAAAAi";s:12:"REDIRECT_URL";s:17:"/admin/index.html";s:11:"REMOTE_ADDR";s:12:"157.86.65.93";s:11:"REMOTE_PORT";s:5:"56960";s:14:"REQUEST_METHOD";s:4:"POST";s:11:"REQUEST_URI";s:17:"/admin/index.html";s:15:"SCRIPT_FILENAME";s:34:"/home2/heron/public_html/index.php";s:11:"SCRIPT_NAME";s:10:"/index.php";s:11:"SERVER_ADDR";s:14:"187.108.192.16";s:12:"SERVER_ADMIN";s:23:"webmaster@kengoo.com.br";s:11:"SERVER_NAME";s:21:"www.chronos-ti.com.br";s:11:"SERVER_PORT";s:2:"80";s:15:"SERVER_PROTOCOL";s:8:"HTTP/1.1";s:16:"SERVER_SIGNATURE";s:0:"";s:15:"SERVER_SOFTWARE";s:6:"Apache";s:9:"UNIQUE_ID";s:24:"T6F5X7tswBAAAD8Hd5AAAAAi";s:8:"PHP_SELF";s:10:"/index.php";s:12:"REQUEST_TIME";i:1335982431;s:4:"argv";a:0:{}s:4:"argc";i:0;}', 'a:7:{s:8:"username";s:12:"heronrsantos";s:8:"password";s:8:"b9295993";s:6:"__utma";s:55:"267039964.1054841183.1335450194.1335450194.1335982421.2";s:6:"__utmz";s:70:"267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";s:9:"PHPSESSID";s:32:"88febfa30ffcb08bf731bea1ff83bc7d";s:6:"__utmb";s:25:"267039964.1.10.1335982421";s:6:"__utmc";s:9:"267039964";}', 'a:0:{}', 'a:5:{s:6:"__utma";s:55:"267039964.1054841183.1335450194.1335450194.1335982421.2";s:6:"__utmz";s:70:"267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";s:9:"PHPSESSID";s:32:"88febfa30ffcb08bf731bea1ff83bc7d";s:6:"__utmb";s:25:"267039964.1.10.1335982421";s:6:"__utmc";s:9:"267039964";}', '2012-05-02 18:13:52', NULL, NULL),
(22, 'Problemas ao efetuar login.', 'a:37:{s:14:"CONTENT_LENGTH";s:2:"39";s:12:"CONTENT_TYPE";s:33:"application/x-www-form-urlencoded";s:13:"DOCUMENT_ROOT";s:24:"/home2/heron/public_html";s:17:"GATEWAY_INTERFACE";s:7:"CGI/1.1";s:11:"HTTP_ACCEPT";s:63:"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";s:20:"HTTP_ACCEPT_ENCODING";s:13:"gzip, deflate";s:20:"HTTP_ACCEPT_LANGUAGE";s:26:"pt-br,en-us;q=0.7,en;q=0.3";s:15:"HTTP_CONNECTION";s:10:"keep-alive";s:11:"HTTP_COOKIE";s:237:"__utma=267039964.1054841183.1335450194.1335450194.1335982421.2; __utmz=267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); PHPSESSID=88febfa30ffcb08bf731bea1ff83bc7d; __utmb=267039964.1.10.1335982421; __utmc=267039964";s:9:"HTTP_HOST";s:21:"www.chronos-ti.com.br";s:12:"HTTP_REFERER";s:45:"http://www.chronos-ti.com.br/admin/index.html";s:15:"HTTP_USER_AGENT";s:65:"Mozilla/5.0 (Windows NT 6.1; rv:12.0) Gecko/20100101 Firefox/12.0";s:4:"PATH";s:13:"/bin:/usr/bin";s:16:"PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:12:"QUERY_STRING";s:0:"";s:25:"REDIRECT_PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:15:"REDIRECT_STATUS";s:3:"200";s:18:"REDIRECT_UNIQUE_ID";s:24:"T6F5ZrtswBAAAD8Hd5MAAAAi";s:12:"REDIRECT_URL";s:17:"/admin/index.html";s:11:"REMOTE_ADDR";s:12:"157.86.65.93";s:11:"REMOTE_PORT";s:5:"56960";s:14:"REQUEST_METHOD";s:4:"POST";s:11:"REQUEST_URI";s:17:"/admin/index.html";s:15:"SCRIPT_FILENAME";s:34:"/home2/heron/public_html/index.php";s:11:"SCRIPT_NAME";s:10:"/index.php";s:11:"SERVER_ADDR";s:14:"187.108.192.16";s:12:"SERVER_ADMIN";s:23:"webmaster@kengoo.com.br";s:11:"SERVER_NAME";s:21:"www.chronos-ti.com.br";s:11:"SERVER_PORT";s:2:"80";s:15:"SERVER_PROTOCOL";s:8:"HTTP/1.1";s:16:"SERVER_SIGNATURE";s:0:"";s:15:"SERVER_SOFTWARE";s:6:"Apache";s:9:"UNIQUE_ID";s:24:"T6F5ZrtswBAAAD8Hd5MAAAAi";s:8:"PHP_SELF";s:10:"/index.php";s:12:"REQUEST_TIME";i:1335982438;s:4:"argv";a:0:{}s:4:"argc";i:0;}', 'a:7:{s:8:"username";s:12:"heronrsantos";s:8:"password";s:8:"b9295993";s:6:"__utma";s:55:"267039964.1054841183.1335450194.1335450194.1335982421.2";s:6:"__utmz";s:70:"267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";s:9:"PHPSESSID";s:32:"88febfa30ffcb08bf731bea1ff83bc7d";s:6:"__utmb";s:25:"267039964.1.10.1335982421";s:6:"__utmc";s:9:"267039964";}', 'a:0:{}', 'a:5:{s:6:"__utma";s:55:"267039964.1054841183.1335450194.1335450194.1335982421.2";s:6:"__utmz";s:70:"267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";s:9:"PHPSESSID";s:32:"88febfa30ffcb08bf731bea1ff83bc7d";s:6:"__utmb";s:25:"267039964.1.10.1335982421";s:6:"__utmc";s:9:"267039964";}', '2012-05-02 18:13:58', NULL, NULL),
(23, 'Problemas ao efetuar login.', 'a:37:{s:14:"CONTENT_LENGTH";s:2:"39";s:12:"CONTENT_TYPE";s:33:"application/x-www-form-urlencoded";s:13:"DOCUMENT_ROOT";s:24:"/home2/heron/public_html";s:17:"GATEWAY_INTERFACE";s:7:"CGI/1.1";s:11:"HTTP_ACCEPT";s:63:"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";s:20:"HTTP_ACCEPT_ENCODING";s:13:"gzip, deflate";s:20:"HTTP_ACCEPT_LANGUAGE";s:26:"pt-br,en-us;q=0.7,en;q=0.3";s:15:"HTTP_CONNECTION";s:10:"keep-alive";s:11:"HTTP_COOKIE";s:237:"__utma=267039964.1054841183.1335450194.1335450194.1335982421.2; __utmz=267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); PHPSESSID=88febfa30ffcb08bf731bea1ff83bc7d; __utmb=267039964.1.10.1335982421; __utmc=267039964";s:9:"HTTP_HOST";s:21:"www.chronos-ti.com.br";s:12:"HTTP_REFERER";s:45:"http://www.chronos-ti.com.br/admin/index.html";s:15:"HTTP_USER_AGENT";s:65:"Mozilla/5.0 (Windows NT 6.1; rv:12.0) Gecko/20100101 Firefox/12.0";s:4:"PATH";s:13:"/bin:/usr/bin";s:16:"PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:12:"QUERY_STRING";s:0:"";s:25:"REDIRECT_PHP_INI_SCAN_DIR";s:13:"//home2/heron";s:15:"REDIRECT_STATUS";s:3:"200";s:18:"REDIRECT_UNIQUE_ID";s:24:"T6F5bLtswBAAAD8Hd5YAAAAi";s:12:"REDIRECT_URL";s:17:"/admin/index.html";s:11:"REMOTE_ADDR";s:12:"157.86.65.93";s:11:"REMOTE_PORT";s:5:"56960";s:14:"REQUEST_METHOD";s:4:"POST";s:11:"REQUEST_URI";s:17:"/admin/index.html";s:15:"SCRIPT_FILENAME";s:34:"/home2/heron/public_html/index.php";s:11:"SCRIPT_NAME";s:10:"/index.php";s:11:"SERVER_ADDR";s:14:"187.108.192.16";s:12:"SERVER_ADMIN";s:23:"webmaster@kengoo.com.br";s:11:"SERVER_NAME";s:21:"www.chronos-ti.com.br";s:11:"SERVER_PORT";s:2:"80";s:15:"SERVER_PROTOCOL";s:8:"HTTP/1.1";s:16:"SERVER_SIGNATURE";s:0:"";s:15:"SERVER_SOFTWARE";s:6:"Apache";s:9:"UNIQUE_ID";s:24:"T6F5bLtswBAAAD8Hd5YAAAAi";s:8:"PHP_SELF";s:10:"/index.php";s:12:"REQUEST_TIME";i:1335982444;s:4:"argv";a:0:{}s:4:"argc";i:0;}', 'a:7:{s:8:"username";s:12:"heronrsantos";s:8:"password";s:8:"skar2006";s:6:"__utma";s:55:"267039964.1054841183.1335450194.1335450194.1335982421.2";s:6:"__utmz";s:70:"267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";s:9:"PHPSESSID";s:32:"88febfa30ffcb08bf731bea1ff83bc7d";s:6:"__utmb";s:25:"267039964.1.10.1335982421";s:6:"__utmc";s:9:"267039964";}', 'a:0:{}', 'a:5:{s:6:"__utma";s:55:"267039964.1054841183.1335450194.1335450194.1335982421.2";s:6:"__utmz";s:70:"267039964.1335450194.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)";s:9:"PHPSESSID";s:32:"88febfa30ffcb08bf731bea1ff83bc7d";s:6:"__utmb";s:25:"267039964.1.10.1335982421";s:6:"__utmc";s:9:"267039964";}', '2012-05-02 18:14:04', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura stand-in para visualizar `historico_view`
--
CREATE TABLE IF NOT EXISTS `historico_view` (
`id` int(11)
,`acao` varchar(255)
,`server_variables` mediumtext
,`request_variables` mediumtext
,`session_variables` mediumtext
,`cookie_variables` mediumtext
,`data` timestamp
,`usuarios_id` int(11)
,`adm_content_id` int(11)
,`data_br` varchar(25)
,`usuarios_nome` varchar(255)
,`adm_content_titulo` varchar(45)
);
-- --------------------------------------------------------

--
-- Estrutura stand-in para visualizar `lixeira`
--
CREATE TABLE IF NOT EXISTS `lixeira` (
`id` int(11)
,`tabela` varchar(11)
,`nome` varchar(255)
,`dataexclusao` varchar(25)
);
-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE IF NOT EXISTS `produtos` (
  `id` int(11) NOT NULL auto_increment,
  `produto` varchar(45) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `produto`) VALUES
(1, 'Cobrança'),
(2, 'Folha de Pagamento'),
(3, 'Pagamentos'),
(4, 'Tributos');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos_has_chamados`
--

CREATE TABLE IF NOT EXISTS `produtos_has_chamados` (
  `produtos_id` int(11) NOT NULL,
  `chamados_id` int(11) NOT NULL,
  PRIMARY KEY  (`produtos_id`,`chamados_id`),
  KEY `fk_produtos_has_chamados_chamados1` (`chamados_id`),
  KEY `fk_produtos_has_chamados_produtos1` (`produtos_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `produtos_has_chamados`
--

INSERT INTO `produtos_has_chamados` (`produtos_id`, `chamados_id`) VALUES
(2, 1),
(3, 1),
(4, 1),
(1, 2),
(2, 2),
(3, 3),
(4, 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `servicos`
--

CREATE TABLE IF NOT EXISTS `servicos` (
  `id` int(11) NOT NULL auto_increment,
  `servico` varchar(45) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `servicos`
--

INSERT INTO `servicos` (`id`, `servico`) VALUES
(1, 'Implantação'),
(2, 'Manutenção'),
(3, 'Orientação');

-- --------------------------------------------------------

--
-- Estrutura da tabela `servicos_has_chamados`
--

CREATE TABLE IF NOT EXISTS `servicos_has_chamados` (
  `servicos_id` int(11) NOT NULL,
  `chamados_id` int(11) NOT NULL,
  PRIMARY KEY  (`servicos_id`,`chamados_id`),
  KEY `fk_servicos_has_chamados_chamados1` (`chamados_id`),
  KEY `fk_servicos_has_chamados_servicos1` (`servicos_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `servicos_has_chamados`
--

INSERT INTO `servicos_has_chamados` (`servicos_id`, `chamados_id`) VALUES
(2, 1),
(3, 1),
(1, 2),
(3, 2),
(2, 3),
(3, 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL auto_increment,
  `usuario` varchar(45) NOT NULL,
  `senha` varchar(45) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `email` varchar(75) NOT NULL,
  `avatar` varchar(45) default NULL,
  `status` char(1) default '1',
  `datacadastro` datetime default NULL,
  `datamodificado` timestamp NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `adm_content_nivel_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `usuario_UNIQUE` (`usuario`),
  KEY `fk_usuarios_adm_content_nivel1` (`adm_content_nivel_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `senha`, `nome`, `email`, `avatar`, `status`, `datacadastro`, `datamodificado`, `adm_content_nivel_id`) VALUES
(1, 'heron', 'd12e69c36157d034edac61831fb236fc', 'Heron Santos', 'heronrsantos@gmail.com', NULL, '1', '2012-03-07 11:41:44', '2012-03-09 10:49:18', 1),
(2, 'junior', '3f875fa9e3fe9d60246da6e7e8ca8f5a', 'Delci Procópio', 'junior@chronos-ti.com.br', NULL, '1', '2012-03-07 11:42:10', '2012-03-07 22:47:05', 2),
(3, 'boavista', 'e10adc3949ba59abbe56e057f20f883e', 'Boa Vista', 'renata.rocha@boavistanet.com.br', NULL, '1', '2012-03-09 10:00:22', '2012-03-13 11:17:22', 5),
(4, 'estela', '1a8e55aca9a73cbe6bdff88c78b7f610', 'Estela de Paula', 'estela@chronos-ti.com.br', NULL, '1', '2012-03-09 10:01:06', '2012-03-09 08:01:06', 3),
(5, 'aurelio', 'c5c4253157eb78de90373d2e885fe719', 'Aurélio Gomez', 'aurelio@chronos-ti.com.br', NULL, '1', '2012-03-09 10:01:47', '2012-03-13 11:50:10', 4);

-- --------------------------------------------------------

--
-- Estrutura stand-in para visualizar `usuarios_view`
--
CREATE TABLE IF NOT EXISTS `usuarios_view` (
`id` int(11)
,`usuario` varchar(45)
,`senha` varchar(45)
,`nome` varchar(255)
,`email` varchar(75)
,`avatar` varchar(45)
,`status` char(1)
,`datacadastro` datetime
,`datamodificado` timestamp
,`adm_content_nivel_id` int(11)
,`datacadastro_br` varchar(25)
,`datamodificado_br` varchar(25)
,`adm_content_nivel_titulo` varchar(45)
);
-- --------------------------------------------------------

--
-- Estrutura para visualizar `adm_content_view`
--
DROP TABLE IF EXISTS `adm_content_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`heron_master`@`187.108.192.16` SQL SECURITY DEFINER VIEW `adm_content_view` AS select `t1`.`id` AS `id`,`t1`.`content` AS `content`,`t1`.`titulo` AS `titulo`,`t1`.`ordenacao` AS `ordenacao`,`t1`.`status` AS `status`,`t1`.`datacadastro` AS `datacadastro`,`t1`.`datamodificado` AS `datamodificado`,`t1`.`adm_content_id` AS `adm_content_id`,`t1`.`adm_content_nivel_id` AS `adm_content_nivel_id`,date_format(`t1`.`datacadastro`,_utf8'%d/%m/%Y às %H:%i') AS `datacadastro_br`,date_format(`t1`.`datamodificado`,_utf8'%d/%m/%Y às %H:%i') AS `datamodificado_br`,`j1`.`titulo` AS `adm_content_nivel_titulo` from (`adm_content` `t1` left join `adm_content_nivel` `j1` on((`j1`.`id` = `t1`.`adm_content_nivel_id`))) where (`t1`.`status` > 0);

-- --------------------------------------------------------

--
-- Estrutura para visualizar `chamados_produtos_view`
--
DROP TABLE IF EXISTS `chamados_produtos_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`heron_master`@`187.108.192.16` SQL SECURITY DEFINER VIEW `chamados_produtos_view` AS select `t1`.`produtos_id` AS `produtos_id`,`t1`.`chamados_id` AS `chamados_id`,`j1`.`produto` AS `produto` from (`produtos_has_chamados` `t1` left join `produtos` `j1` on((`j1`.`id` = `t1`.`produtos_id`)));

-- --------------------------------------------------------

--
-- Estrutura para visualizar `chamados_servicos_view`
--
DROP TABLE IF EXISTS `chamados_servicos_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`heron_master`@`187.108.192.16` SQL SECURITY DEFINER VIEW `chamados_servicos_view` AS select `t1`.`servicos_id` AS `servicos_id`,`t1`.`chamados_id` AS `chamados_id`,`j1`.`servico` AS `servico` from (`servicos_has_chamados` `t1` left join `servicos` `j1` on((`j1`.`id` = `t1`.`servicos_id`)));

-- --------------------------------------------------------

--
-- Estrutura para visualizar `chamados_view`
--
DROP TABLE IF EXISTS `chamados_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`heron_master`@`187.108.192.16` SQL SECURITY DEFINER VIEW `chamados_view` AS select `t1`.`id` AS `id`,`t1`.`numero` AS `numero`,`t1`.`empresa` AS `empresa`,`t1`.`cnpj` AS `cnpj`,`t1`.`telefone` AS `telefone`,`t1`.`endereco_logradouro` AS `endereco_logradouro`,`t1`.`endereco_complemento` AS `endereco_complemento`,`t1`.`endereco_bairro` AS `endereco_bairro`,`t1`.`endereco_cidade` AS `endereco_cidade`,`t1`.`endereco_estado` AS `endereco_estado`,`t1`.`endereco_cep` AS `endereco_cep`,`t1`.`anotacoes` AS `anotacoes`,`t1`.`agenda_data` AS `agenda_data`,`t1`.`agenda_hora_ini` AS `agenda_hora_ini`,`t1`.`agenda_hora_fim` AS `agenda_hora_fim`,`t1`.`atendimento_data` AS `atendimento_data`,`t1`.`atendimento_hora_ini` AS `atendimento_hora_ini`,`t1`.`atendimento_hora_fim` AS `atendimento_hora_fim`,`t1`.`visita_status` AS `visita_status`,`t1`.`tecnico` AS `tecnico`,`t1`.`observacao` AS `observacao`,`t1`.`status` AS `status`,`t1`.`datacadastro` AS `datacadastro`,`t1`.`datamodificado` AS `datamodificado`,`t1`.`bancos_id` AS `bancos_id`,`t1`.`usuarios_id` AS `usuarios_id`,date_format(`t1`.`agenda_data`,_utf8'%d/%m/%Y') AS `agenda`,date_format(`t1`.`atendimento_data`,_utf8'%d/%m/%Y') AS `atendimento`,concat(date_format(`t1`.`agenda_data`,_utf8'%d/%m/%Y'),_utf8' ',date_format(`t1`.`agenda_hora_ini`,_utf8'%H:%i'),_utf8'~',date_format(`t1`.`agenda_hora_fim`,_utf8'%H:%i')) AS `agenda_br`,concat(date_format(`t1`.`atendimento_data`,_utf8'%d/%m/%Y'),_utf8' ',date_format(`t1`.`atendimento_hora_ini`,_utf8'%H:%i'),_utf8'~',date_format(`t1`.`atendimento_hora_fim`,_utf8'%H:%i')) AS `atendimento_br`,date_format(`t1`.`datacadastro`,_utf8'%d/%m/%Y às %H:%i') AS `datacadastro_br`,date_format(`t1`.`datamodificado`,_utf8'%d/%m/%Y às %H:%i') AS `datamodificado_br`,`j1`.`banco` AS `banco`,`j2`.`nome` AS `usuarios_nome`,if((`t1`.`status` = _utf8'1'),_utf8'Aberta',if(`t1`.`status`,_utf8'Fechada',_utf8'')) AS `status_chamado` from ((`chamados` `t1` left join `bancos` `j1` on((`j1`.`id` = `t1`.`bancos_id`))) left join `usuarios_view` `j2` on((`j2`.`id` = `t1`.`usuarios_id`))) where (`t1`.`status` > 0);

-- --------------------------------------------------------

--
-- Estrutura para visualizar `historico_view`
--
DROP TABLE IF EXISTS `historico_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`heron_master`@`187.108.192.16` SQL SECURITY DEFINER VIEW `historico_view` AS select `t1`.`id` AS `id`,`t1`.`acao` AS `acao`,`t1`.`server_variables` AS `server_variables`,`t1`.`request_variables` AS `request_variables`,`t1`.`session_variables` AS `session_variables`,`t1`.`cookie_variables` AS `cookie_variables`,`t1`.`data` AS `data`,`t1`.`usuarios_id` AS `usuarios_id`,`t1`.`adm_content_id` AS `adm_content_id`,date_format(`t1`.`data`,_utf8'%d/%m/%Y às %H:%i') AS `data_br`,`j2`.`nome` AS `usuarios_nome`,`j1`.`titulo` AS `adm_content_titulo` from ((`historico` `t1` left join `adm_content_view` `j1` on((`j1`.`id` = `t1`.`adm_content_id`))) left join `usuarios_view` `j2` on((`j2`.`id` = `t1`.`usuarios_id`)));

-- --------------------------------------------------------

--
-- Estrutura para visualizar `lixeira`
--
DROP TABLE IF EXISTS `lixeira`;

CREATE ALGORITHM=UNDEFINED DEFINER=`heron_master`@`187.108.192.16` SQL SECURITY DEFINER VIEW `lixeira` AS (select `t1`.`id` AS `id`,_utf8'usuarios' AS `tabela`,`t1`.`nome` AS `nome`,date_format(`t1`.`datamodificado`,_utf8'%d/%m/%Y às %H:%i') AS `dataexclusao` from `usuarios` `t1` where (`t1`.`status` > 0)) union (select `t1`.`id` AS `id`,_utf8'adm_content' AS `tabela`,`t1`.`titulo` AS `nome`,date_format(`t1`.`datamodificado`,_utf8'%d/%m/%Y às %H:%i') AS `dataexclusao` from `adm_content` `t1` where (`t1`.`status` > 0));

-- --------------------------------------------------------

--
-- Estrutura para visualizar `usuarios_view`
--
DROP TABLE IF EXISTS `usuarios_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`heron_master`@`187.108.192.16` SQL SECURITY DEFINER VIEW `usuarios_view` AS select `t1`.`id` AS `id`,`t1`.`usuario` AS `usuario`,`t1`.`senha` AS `senha`,`t1`.`nome` AS `nome`,`t1`.`email` AS `email`,`t1`.`avatar` AS `avatar`,`t1`.`status` AS `status`,`t1`.`datacadastro` AS `datacadastro`,`t1`.`datamodificado` AS `datamodificado`,`t1`.`adm_content_nivel_id` AS `adm_content_nivel_id`,date_format(`t1`.`datacadastro`,_utf8'%d/%m/%Y às %H:%i') AS `datacadastro_br`,date_format(`t1`.`datamodificado`,_utf8'%d/%m/%Y às %H:%i') AS `datamodificado_br`,`j1`.`titulo` AS `adm_content_nivel_titulo` from (`usuarios` `t1` left join `adm_content_nivel` `j1` on((`j1`.`id` = `t1`.`adm_content_nivel_id`))) where (`t1`.`status` > 0);

--
-- Restrições para as tabelas dumpadas
--

--
-- Restrições para a tabela `adm_content`
--
ALTER TABLE `adm_content`
  ADD CONSTRAINT `fk_adm_content_adm_content1` FOREIGN KEY (`adm_content_id`) REFERENCES `adm_content` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_adm_content_adm_content_nivel1` FOREIGN KEY (`adm_content_nivel_id`) REFERENCES `adm_content_nivel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `chamados`
--
ALTER TABLE `chamados`
  ADD CONSTRAINT `fk_chamados_bancos1` FOREIGN KEY (`bancos_id`) REFERENCES `bancos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_chamados_usuarios1` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `historico`
--
ALTER TABLE `historico`
  ADD CONSTRAINT `fk_historico_adm_content1` FOREIGN KEY (`adm_content_id`) REFERENCES `adm_content` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_historico_usuarios1` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `produtos_has_chamados`
--
ALTER TABLE `produtos_has_chamados`
  ADD CONSTRAINT `fk_produtos_has_chamados_chamados1` FOREIGN KEY (`chamados_id`) REFERENCES `chamados` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_produtos_has_chamados_produtos1` FOREIGN KEY (`produtos_id`) REFERENCES `produtos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `servicos_has_chamados`
--
ALTER TABLE `servicos_has_chamados`
  ADD CONSTRAINT `fk_servicos_has_chamados_chamados1` FOREIGN KEY (`chamados_id`) REFERENCES `chamados` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_servicos_has_chamados_servicos1` FOREIGN KEY (`servicos_id`) REFERENCES `servicos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_usuarios_adm_content_nivel1` FOREIGN KEY (`adm_content_nivel_id`) REFERENCES `adm_content_nivel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
